//
//  LAMMPSSystem.cpp
//  ParSplice-refact
//
//  Created by Danny Perez on 11/17/16.
//  Copyright © 2016 dp. All rights reserved.
//
//  LAMMPS-specific content added by Steve Plimpton, Dec 2016
//


#include <cassert>
#include <map>
#include <numeric>
#include <algorithm>
#include <iostream>

#include <boost/lexical_cast.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <boost/iostreams/device/file.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/device/back_inserter.hpp>


#include "LAMMPSSystem.hpp"


// --------------------------------------------------------
// required methods
// --------------------------------------------------------

LAMMPSSystem::LAMMPSSystem(){
};

// --------------------------------------------------------

LAMMPSSystem::~LAMMPSSystem(){
};


int LAMMPSSystem::getNAtoms()
{
	return natoms;
};




void LAMMPSSystem::setNAtoms(int n)
{
	clearAtoms();

	natoms = n;
	id = std::vector<int>(natoms,0);
	species = std::vector<int>(natoms,0);
	x = std::vector<double>(NDIM*natoms,0);
	v = std::vector<double>(NDIM*natoms,0);
	image = std::vector<int>(natoms,0);
	q = std::vector<double>(natoms,0);
	molecule = std::vector<int>(natoms,0);
};


void LAMMPSSystem::resize(int n)
{
	natoms = n;
	id.resize(n);
	species.resize(n);
	x.resize(NDIM*n);
	v.resize(NDIM*n);
	image.resize(n);
	q.resize(n);
	molecule.resize(n);
};


void LAMMPSSystem::clearAtoms()
{
	natoms = 0;

	id.clear();
	species.clear();
	x.clear();
	v.clear();
	image.clear();
	q.clear();
	molecule.clear();
};

// --------------------------------------------------------










// --------------------------------------------------------

double LAMMPSSystem::getBox(int iDim, int jDim)
{
	return boxabc[iDim][jDim];
}

void LAMMPSSystem::setBox(int iDim, int jDim, double value)
{
	boxabc[iDim][jDim] = value;
}

// --------------------------------------------------------

int LAMMPSSystem::getPeriodic(int iDim)
{
	return periodicity[iDim];
}

void LAMMPSSystem::setPeriodic(int iDim, bool value)
{
	periodicity[iDim] = (value ? 1 : 0);
}

// --------------------------------------------------------

void LAMMPSSystem::setUniqueID(int iAtom, int value)
{
	assert(iAtom < natoms);
	id[iAtom] = value;
}

int LAMMPSSystem::getUniqueID(int iAtom)
{
	assert(iAtom < natoms);
	return id[iAtom];
}

// --------------------------------------------------------

void LAMMPSSystem::setSpecies(int iAtom, int value)
{
	assert(iAtom < natoms);
	species[iAtom] = value;
}

int LAMMPSSystem::getSpecies(int iAtom)
{
	assert(iAtom < natoms);
	return species[iAtom];
}

// --------------------------------------------------------

void LAMMPSSystem::setPosition(int iAtom, int iDim, double value)
{
	int index = iAtom*NDIM + iDim;
	assert(index < natoms*NDIM);
	x[index] = value;
}

double LAMMPSSystem::getPosition(int iAtom, int iDim)
{
	int index = iAtom*NDIM + iDim;
	assert(index < natoms*NDIM);
	return x[index];
}

// --------------------------------------------------------

void LAMMPSSystem::setVelocity(int iAtom, int iDim, double value)
{
	int index = iAtom*NDIM + iDim;
	assert(index < natoms*NDIM);
	v[index] = value;
}

double LAMMPSSystem::getVelocity(int iAtom, int iDim)
{
	int index = iAtom*NDIM + iDim;
	assert(index < natoms*NDIM);
	return v[index];
}



// --------------------------------------------------------
// extra methods
// --------------------------------------------------------

int LAMMPSSystem::getNTimestep()
{
	return ntimestep;
}

void LAMMPSSystem::setNTimestep(int64_t n)
{
	ntimestep = n;
}

// --------------------------------------------------------

void LAMMPSSystem::box2abc()
{
	boxabc[0][0] = boxhi[0]-boxlo[0];
	boxabc[0][1] = 0.0;
	boxabc[0][2] = 0.0;

	boxabc[1][0] = xy;
	boxabc[1][1] = boxhi[1]-boxlo[1];
	boxabc[1][2] = 0.0;

	boxabc[2][0] = xz;
	boxabc[2][1] = yz;
	boxabc[2][2] = boxhi[2]-boxlo[2];
}

// --------------------------------------------------------

void LAMMPSSystem::abc2box()
{
	boxabc[0][0] = boxhi[0]-boxlo[0];
	boxabc[0][1] = 0.0;
	boxabc[0][2] = 0.0;

	boxabc[1][0] = xy;
	boxabc[1][1] = boxhi[1]-boxlo[1];
	boxabc[1][2] = 0.0;

	boxabc[2][0] = xz;
	boxabc[2][1] = yz;
	boxabc[2][2] = boxhi[2]-boxlo[2];
}


int LAMMPSSystem::addAtom(std::vector<double> positions, int species, std::map<std::string, std::string> attributes){
	natoms++;
	resize(natoms);

	setUniqueID(natoms-1,natoms);
	for(int k=0; k<NDIM; k++) {
		setPosition(natoms-1,k,positions[k]);
		setVelocity(natoms-1,k,0);
		setSpecies(natoms-1,species);
		setUniqueID(natoms-1,natoms);
		image[natoms-1]=0;
		//set the charge
		if(attributes.count("q")) {
			q[natoms-1]=boost::lexical_cast<double>(attributes["q"]);
		}
		if(attributes.count("molecule")) {
			molecule[natoms-1]=boost::lexical_cast<double>(attributes["molecule"]);
		}
	}
	return natoms;
};

void LAMMPSSystem::remap(std::map<int,int> newUniqueID){

	/*
	   for(int i=0; i<id.size(); i++) {
	        std::cout<<id[i]<<std::endl;
	   }
	   std::cout<<"================="<<std::endl;
	 */

	int nAtoms=getNAtoms();
	for(int i=0; i<nAtoms; i++) {
		setUniqueID(i, newUniqueID[ getUniqueID(i)]);
	}

	std::vector<int> asort(id.size());
	std::vector<int> ids=id;
	std::iota(asort.begin(), asort.end(), 0);
	auto comparator = [&ids](int a, int b){
				  return ids[a] < ids[b];
			  };
	std::sort(asort.begin(), asort.end(), comparator);

	std::vector<int> tid=id;
	std::vector<int> tsp=species;
	std::vector<int> tim=image;
	std::vector<double> tq=q;
	std::vector<int> tmolecule=molecule;
	std::vector<double> tx=x;
	std::vector<double> tv=v;
	for(int i = 0; i < ids.size(); ++i)  {
		id[i]=tid[asort[i]];
		species[i]=tsp[asort[i]];
		image[i]=tim[asort[i]];
		q[i]=tq[asort[i]];
		molecule[i]=tmolecule[asort[i]];
		for(int j=0; j<NDIM; j++) {
			x[NDIM*i+j] = tx[ NDIM*asort[i]+j ];
			v[NDIM*i+j] = tv[ NDIM*asort[i]+j ];
		}
	}
};
