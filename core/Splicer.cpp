/*
 Copyright (c) 2016, Los Alamos National Security, LLC
 All rights reserved.
 Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

 Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "Splicer.hpp"

ParSpliceSplicer::ParSpliceSplicer(boost::property_tree::ptree &config) : modifier(config){ };

ParSpliceSplicer::ParSpliceSplicer(MPI_Comm splicerComm_, AbstractDDS *minimaStore_, std::set<int> children_, boost::property_tree::ptree &config, MPI_Comm workerComm) : modifier(config){

  //BOOST_LOG_SEV(lg,debug) <<"#Splicer() ";
  splicerComm=splicerComm_;
  usedBlocks=unusedBlocks=wastedBlocks=producedBlocks=0;
  carryOverTime=0;
  minimaStore=minimaStore_;
  children=children_;
  validator.init(children,1);

  start=std::chrono::high_resolution_clock::now();
  die=false;
  killed=false;

  officialTrajectory.nKMCPred = 0;

  //check if checkpoint files are present
  bool restartFromCheckPoint= boost::filesystem::exists("./traj.out.chk") && boost::filesystem::exists("./times.out.chk") && boost::filesystem::exists("./Splicer.chk");
  //BOOST_LOG_SEV(lg,debug) <<"#Splicer() restartFromCheckPoint: "<<restartFromCheckPoint;

  if(restartFromCheckPoint) {
    boost::filesystem::copy_file("./traj.out.chk","./traj.out",boost::filesystem::copy_option::overwrite_if_exists);
    boost::filesystem::copy_file("./times.out.chk","./times.out",boost::filesystem::copy_option::overwrite_if_exists);
    outTraj.open("./traj.out", std::ios::app);
    outTime.open("./times.out", std::ios::app);
  }
  else{
    outTraj.open("./traj.out", std::ios::out);
    outTime.open("./times.out", std::ios::out);
  }

  //initialize from config data
  outputvol=config.get<unsigned>("ParSplice.Splicer.OutputVol",OUTPUTVOL_DEF);
  wMin=config.get<double>("ParSplice.MC.MinimumTrajectoryWeigth",WMIN_DEF);
  defaultVisitDuration=config.get<unsigned>("ParSplice.MC.DefaultVisitDuration",VISDUR_DEF);
  predictionGranularity=config.get<unsigned>("ParSplice.MC.PredictionGranularity",PREDGRAN_DEF);
  batchSize=config.get<unsigned>("ParSplice.Topology.NWorkers",NWORKER_DEF);
  predictionTime=config.get<unsigned>("ParSplice.Topology.NWorkers",NWORKER_DEF);
  maxTaskMesgSize=config.get<int>("ParSplice.MaximumTaskMesgSize",MXTASKMSG_DEF);
  validatorTimeout=config.get<unsigned>("ParSplice.Splicer.ValidatorTimeout",VALTOUT_DEF);

  broadcastDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.BroadcastDelay",BCASTDELAY_DEF) );
  reportDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.ReportDelay",RPRTDELAY_DEF) );
  checkpointDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.CheckpointDelay",CHKDELAY_DEF) );
  flushOnModify= config.get<bool>("ParSplice.Splicer.FlushOnModify",FLUSHONMOD_DEF);
  runTime=std::chrono::minutes( config.get<unsigned>("ParSplice.RunTime",RUNTIME_DEF) );

  //read task parameters
  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, config.get_child("ParSplice.TaskParameters")) {
    std::string stype=v.second.get<std::string>("Type");
    boost::trim(stype);
    int type=taskTypeByLabel.at(stype);
    int flavor=v.second.get<int>("Flavor");
    BOOST_FOREACH(boost::property_tree::ptree::value_type &vv, v.second.get_child("")) {
      std::string key=vv.first;
      std::string data=vv.second.data();
      boost::trim(key);
      boost::trim(data);
      std::cout<<key<<" "<<data<<std::endl;
      taskParameters[std::make_pair(type,flavor)][key]=data;
    }
  }

  driver=new DriverHandleType(workerComm);

  //TODO: FIX ME
  defaultFlavor=1;
  STATE_DBKEY=0;
  batchId=1;
  broadcastRequests=std::vector<MPI_Request>(children.size(),MPI_REQUEST_NULL);

  if(restartFromCheckPoint) {
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer(): Restoring checkpoing ";
    // create and open an archive for input
    std::ifstream ifs("Splicer.chk");
    boost::archive::text_iarchive ia(ifs);
    // read class state from archive
    ia >> *this;
    // archive and stream closed when destructors are called
  }
  else{
    addsystem_byfile(config);
  }

  std::cout<<"INITIAL TRAJECTORY: "<<std::endl;
  officialTrajectory.print();

};

ParSpliceSplicer::~ParSpliceSplicer(){
  delete driver;
};

void ParSpliceSplicer::addsystem_byfile(boost::property_tree::ptree &config){

  TaskType task;

  std::string initialConfiguration=config.get<std::string>("ParSplice.InitialConfiguration");
  boost::trim(initialConfiguration);

  task.type=PARSPLICE_TASK_INIT_FROM_FILE;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_INIT_FROM_FILE,defaultFlavor)];
  task.parameters["Filename"]=initialConfiguration;
  driver->assign(task);
  while(not driver->probe(task)) {};
  task.parameters.clear();

  task.type=PARSPLICE_TASK_MIN;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MIN,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};
  task.systems[0].type=TYPE::MINIMUM;

  task.type=PARSPLICE_TASK_LABEL;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_LABEL,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};

  task.type=PARSPLICE_TASK_REMAP;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_REMAP,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};

  Rd data;
  pack(data,task.systems[0]);
  minimaStore->put(STATE_DBKEY,task.systems[0].label,data);
  std::cout<<"DB STORE "<<data.size()<<" "<<task.systems[0].label<<std::endl;

  Visit v;
  v.label=task.systems[0].label;
  v.duration=0;
  officialTrajectory.appendVisit(v);
  officialTrajectory.flavor=defaultFlavor;

};

void ParSpliceSplicer::server(){
  //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server ";

  lastBroadcast=std::chrono::high_resolution_clock::now();
  lastReport=std::chrono::high_resolution_clock::now();
  lastCheckpoint=std::chrono::high_resolution_clock::now();

  //TODO: do this right
  rbufSize=100000000;
  rbuf=std::vector<char>(rbufSize,' ');

  //post a  non-blocking receive
  MPI_Irecv(&(rbuf[0]),rbufSize,MPI_BYTE,MPI_ANY_SOURCE,MPI_ANY_TAG,splicerComm,&incomingRequest);
  recvCompleted=0;

  //std::vector<std::vector<char> > sBuf(children.size());
  officialTrajectory.nKMCPred=0;

  while(true) {

    //std::cout<<"updateStores(); "<<std::endl<<std::flush;
    //update databases
    updateStores();

    //std::cout<<"processRecv(); "<<std::endl<<std::flush;
    //process non-blocking receives (completed segments from WM)
    processRecv();

    //std::cout<<"multiSplice(); "<<std::endl<<std::flush;
    //splice active trajectories as much as possible
    splice();

    //std::cout<<"KMC(); "<<std::endl<<std::flush;
    //update KMC predictors
    KMC();

    //std::cout<<"processSend(); "<<std::endl<<std::flush;
    //process non-blocking sends (tasks to WM)
    processSend();

    //std::cout<<"report(); "<<std::endl<<std::flush;
    //write outputs if needed
    report();

    //std::cout<<"checkpoint(); "<<std::endl<<std::flush;
    //checkpoint if needed
    checkpoint();


    if(killed) {
      std::cout<<"Splicer: Die signal has been sent. Shutting down."<<std::endl;

      //kill our own worker
      TaskType task;
      task.type=PARSPLICE_TASK_DIE;
      task.flavor=defaultFlavor;
      task.batchId=batchId;
      task.systems.clear();
      driver->assign(task);

      //force a checkpoint
      //checkpoint(true);
      break;
    }
    if(std::chrono::high_resolution_clock::now() - start > runTime  ) {
      die=true;
      std::cout<<"Splicer: Time to shut down"<<std::endl;
    }

  }

};

void ParSpliceSplicer::updateStores(){
  int imax=1000;
  int i=0;
  while(minimaStore->singleServe()>0 and i<imax) {
    i++;
  }
};

void ParSpliceSplicer::processRecv(){
  //did we receive new completed tasks?

  if(die) {
    std::cout<<"Splicer: cancelling pending receives"<<std::endl;
    MPI_Cancel(&incomingRequest);
    MPI_Wait(&incomingRequest,&recvStatus);
  }
  else{

    if(not recvCompleted) {
      MPI_Test(&incomingRequest,&recvCompleted,&recvStatus);
    }
    if(recvCompleted) {
      Timer t;
      //extract
      int peer=recvStatus.MPI_SOURCE;
      int tag=recvStatus.MPI_TAG;
      int count=0;
      MPI_Get_count( &recvStatus, MPI_BYTE, &count );

      //std::cout<<"SPLICER RECEIVED "<<count<<" BYTES FROM "<<peer<<" WITH TAG "<<tag<<" "<<std::endl;
      //std::vector<char> rrbuf(rbuf.begin(),rbuf.begin()+count);
      //std::cout<<"HASH: "<<boost::hash_value(rrbuf)<<" SIZE: :"<<count<<std::endl;
      //std::cout<<int(rrbuf[0])<<" "<<int(rrbuf[rrbuf.size()-1])<<std::endl;

      switch (tag) {
        case PARSPLICE_STATS_SEGMENTS_TAG: {
          //std::cout<<"SPLICER RECEIVED SEGMENTS AND STATS"<<std::endl;

          //SegmentDatabase incomingSegments;
          std::list<SegmentDatabase> incomingBatches;
          TransitionStatistics statistics;
          unpack(rbuf,statistics,incomingBatches,std::size_t(count));

          for(auto itbatch=incomingBatches.begin(); itbatch!=incomingBatches.end(); itbatch++) {
            for(auto itdb=itbatch->db.begin(); itdb!=itbatch->db.end(); itdb++) {
              for(auto ittraj=itdb->second.begin(); ittraj!=itdb->second.end(); ittraj++) {
                producedBlocks+=ittraj->duration();
              }
            }
          }

          //std::cout<<"SPLICER RECEIVED SEGMENTS AND STATS "<<incomingBatches.size()<<" "<<statistics.size()<<std::endl;
          //validate the new batches
          for(auto it=incomingBatches.begin(); it!=incomingBatches.end(); it++) {
            //std::cout<<"SPLICER VALIDATED SEGMENTS "<<std::endl;
            //it->print();
            validator.validate(peer,*it);
          }
          //std::cout<<"SPLICER VALIDATED SEGMENTS "<<std::endl;

          //transfer to segmentDB
          while(true) {
            validator.refreshHead(validatorTimeout);
            SegmentDatabase validatedSegments=validator.release();
            if(validatedSegments.size()>0) {
              //std::cout<<"SPLICER MERGING SEGMENTS "<<std::endl;
              //validatedSegments.print();
              segmentDB.merge(validatedSegments);
            }
            else{
              break;
            }
          }

          //std::cout<<"SPLICER MERGED SEGMENTS "<<std::endl;
          //update Markov model
          markovModel.assimilate(statistics);
          //std::cout<<"SPLICER UPDATED MARKOV MODEL "<<std::endl;

          break;
        }

        default: {
          //We don't support any other task yet
          assert(tag==PARSPLICE_STATS_SEGMENTS_TAG or tag==PARSPLICE_SYNC_STATS_TAG);
          break;
        }

      }

      //post a new receive for tasks
      MPI_Irecv(&(rbuf[0]),rbufSize,MPI_BYTE,MPI_ANY_SOURCE,MPI_ANY_TAG,splicerComm,&incomingRequest);
      recvCompleted=0;

    }
  }
};

void ParSpliceSplicer::processSend(){

  //std::cout<<"SPLICER DONE KMC "<<std::endl;
  //test for completion of the previous task broadcast
  MPI_Testall(children.size(), &(broadcastRequests[0]),&broadcastCompleted, MPI_STATUSES_IGNORE);

  if(die and not killed) {
    std::cout<<"Splicer: killing the workers"<<std::endl;
    MPI_Waitall(children.size(), &(broadcastRequests[0]),MPI_STATUSES_IGNORE);

    TaskDescriptorBundle taskBundle;
    TaskDescriptor task;
    task.type=PARSPLICE_TASK_DIE;
    task.flavor=defaultFlavor;
    task.batchId=batchId;
    task.nInstances=1;
    task.systems.clear();
    taskBundle.insert(task);

    int iChild=0;
    for(auto it=children.begin(); it!=children.end(); it++,iChild++) {
      pack(sBuf[iChild],taskBundle);
      //TODO make this more robust
      assert(sBuf[iChild].size()<maxTaskMesgSize);
      MPI_Issend(&(sBuf[iChild][0]),sBuf[iChild].size(),MPI_BYTE,*it,PARSPLICE_TASK_TAG,splicerComm, &(broadcastRequests[iChild]));
    }
    killed=true;

    MPI_Waitall(children.size(), &(broadcastRequests[0]),MPI_STATUSES_IGNORE);
  }

  //buffer the predictions and broadcast only if delay has passed since the last time
  //also make sure we are done broadcasting the previous batch
  if(std::chrono::high_resolution_clock::now() - lastBroadcast> broadcastDelay and broadcastCompleted) {

    Timer t;
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server broadcasting ";
    std::cout<<"SPLICER BROADCASTING"<<std::endl;
    std::cout<<"CURRENT HEAD(S): "<<std::endl;
    std::cout<<officialTrajectory.back().label <<std::endl;

    //reset the send buffer
    sBuf=std::vector<std::vector<char> >(children.size());

    {

      //TODO: make this faster
      std::map<Label, unsigned > counts;
      int nKeep=0;

      for(auto it=officialTrajectory.predm.begin(); it!=officialTrajectory.predm.end(); it++) {
        it->second.erase(0);
      }

      std::set<SchedulerState> schedulingSet;
      for(auto it=officialTrajectory.predm.begin(); it!=officialTrajectory.predm.end(); it++) {
        SchedulerState s;
        s.label=it->first;
        s.counts=it->second;
        if(s.counts.size()>0) {
          schedulingSet.insert(s);
        }
      }

      while(nKeep<batchSize and schedulingSet.size()>0) {
        //find the state with the largest number of KMC instances that required more segments
        auto it=schedulingSet.rbegin();

        SchedulerState s=*it;
        //remove from the set
        schedulingSet.erase(std::next(it).base());
        for(int i=0; i<predictionGranularity; i++) {
          nKeep++;
          counts[s.label]+=1;
          s.counts.erase(counts[s.label]);
        }
        //reinsert if there are KMC instances left
        if(s.counts.size()>0) {
          //hint that the updated state might still be close to the back
          schedulingSet.insert(schedulingSet.cend(),s);
        }
      }

      //clear the previous predictions
      officialTrajectory.predm.clear();
      officialTrajectory.nKMCPred=0;

      //split the load between children
      //TODO: This has to be improved to consider affinity
      unsigned nTaskPerChild=(children.size()>0 ? nKeep/children.size() : 0);
      unsigned np=nKeep%children.size();

      //pack and send the messages
      int iChild=0;
      for(auto it=children.begin(); it!=children.end(); it++,iChild++) {

        //create the task bundle
        TaskDescriptorBundle taskBundle;
        TaskDescriptor task;
        task.type=PARSPLICE_TASK_SEGMENT;
        task.flavor=defaultFlavor;
        task.batchId=batchId;
        unsigned n=0;
        unsigned nt=nTaskPerChild+(iChild<np ? 1 : 0);
        for(auto itc=counts.begin(); itc!=counts.end(); ) {
          unsigned nInstances=itc->second;
          Label lb=itc->first;

          //we can take in all of these tasks
          if(n+nInstances<nt) {
            n+=nInstances;
            itc=counts.erase(itc);
          }
          //we can only take part of these tasks
          else{
            nInstances=nt-n;
            n=nt;
            itc->second-=nInstances;
          }

          std::cout<<"SCHEDULING "<<nInstances<<" INSTANCES IN STATE " <<lb<<" FOR WORKER "<<*it<<std::endl;

          task.nInstances=nInstances;
          task.systems.clear();
          SystemPlaceholder s;
          s.label=lb;
          s.necessity=NECESSITY::REQUIRED;
          s.type=TYPE::MINIMUM;
          task.systems.push_back(s);
          s.necessity=NECESSITY::OPTIONAL;
          s.type=TYPE::QSD;
          task.systems.push_back(s);

          taskBundle.insert(task);

          if(n==nt) {
            break;
          }
        }
        //sBuf[iChild].clear();
        pack(sBuf[iChild],taskBundle);
        //std::cout<<splicerComm<<" "<<&(sBuf[iChild][0])<<" "<<sBuf[iChild].size()<<" "<<*it<<" "<<PARSPLICE_TASK_TAG <<" "<< &(broadcastRequests[iChild])<<std::endl;

        /*
         {
         //test
         TaskDescriptorBundle tb;
         unpack(sBuf[iChild],tb,sBuf[iChild].size());
         std::cout<<"HASH S "<<boost::hash_range(sBuf[iChild].begin(),sBuf[iChild].end())<<" "<<sBuf[iChild].size()<<std::endl;
         }
         */

        //TODO make this more robust
        assert(sBuf[iChild].size()<maxTaskMesgSize);
        MPI_Issend(&(sBuf[iChild][0]),sBuf[iChild].size(),MPI_BYTE,*it,PARSPLICE_TASK_TAG,splicerComm, &(broadcastRequests[iChild]));
      }
    }
    lastBroadcast=std::chrono::high_resolution_clock::now();

    std::cout<<"SPLICER DONE BROADCASTING "<<t.stop()<<std::endl;
    batchId++;
  }
};


void ParSpliceSplicer::splice(){
  Timer t;
  unsigned long usedBefore;

  Label previousLabel=officialTrajectory.back().label;

  bool spliced=false;
  while(usedBefore=usedBlocks, spliceOne()) {
    spliced=true;
    if(modifier.modificationNeeded(usedBefore,usedBlocks))
    {
      std::cout<<"MODIFY "<<officialTrajectory.back().label<<std::endl;
      Rd data;
      SystemType s;
      uint64_t res=minimaStore->reserve(STATE_DBKEY,officialTrajectory.back().label);
      while(not minimaStore->get(STATE_DBKEY, officialTrajectory.back().label,data)) {minimaStore->singleServe();}
      minimaStore->release(res);
      unpack(data,s,0);

      modifier.modify(s, usedBefore, usedBlocks);

      std::cout<<"MODIFY DONE"<<std::endl;
      TaskType task;
      task.systems.push_back(s);

      task.type=PARSPLICE_TASK_MIN;
      task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MIN,defaultFlavor)];
      driver->assign(task);
      while(not driver->probe(task)) {};
      task.systems[0].type=TYPE::MINIMUM;
      std::cout<<"MIN DONE"<<std::endl;

      task.type=PARSPLICE_TASK_REMAP;
      task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_REMAP,defaultFlavor)];
      driver->assign(task);
      while(not driver->probe(task)) {};
      task.systems[0].type=TYPE::MINIMUM;
      std::cout<<"REMAP DONE "<<task.systems[0].label<<std::endl;

      data.clear();
      pack(data,task.systems[0]);
      minimaStore->put(STATE_DBKEY,task.systems[0].label,data);
      std::cout<<"PUT DONE"<<std::endl;

      Visit v;
      v.label=task.systems[0].label;
      v.duration=0;
      officialTrajectory.appendVisit(v);
      std::cout<<"APPEND DONE"<<std::endl;

      if(flushOnModify) {
        markovModel.clear();
        segmentDB.clear();
      }
    }

  }
  if(officialTrajectory.back().label!=previousLabel) {
    officialTrajectory.predm.clear();
    officialTrajectory.nKMCPred=0;
  }
  if(spliced) {
    //std::cout<<"DONE SPLICING: "<<t.stop()<<std::endl;
  }

};

void ParSpliceSplicer::KMC(){
  if(officialTrajectory.nKMCPred<100) {
    //generate a virtual trajectory
    predictions.clear();
    KMCSchedule(predictions);
    //std::cout<<"CURRENT HEAD "<< officialTrajectory.back().label <<std::endl;
    for(auto it=predictions.begin(); it!=predictions.end(); it++) {
      //std::cout<<"PREDICTION: "<<it->first<<" "<<it->second<<std::endl;
      officialTrajectory.predm[it->first].insert(it->second);
    }
    //std::cout<<"DONE KMC: "<<t.stop()<<std::endl;
  }
  officialTrajectory.nKMCPred++;
};

void ParSpliceSplicer::report(){
  //report at given intervals
  if(std::chrono::high_resolution_clock::now() - lastReport> reportDelay  ) {
    Timer t;
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server reporting ";

    //output timings
    outTime<<std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-start).count()+carryOverTime<<" "<<usedBlocks<<" "<<producedBlocks<<std::endl;

    //output trajectory
    int iVisit=0;
    Visit back=officialTrajectory.back();
    officialTrajectory.pop_back();

    std::stringstream ss;
    for(auto it=officialTrajectory.visits.begin(); it!=officialTrajectory.visits.end(); it++) {
      ss<<" "<<it->label<<" "<<it->duration<<" 0\n"; // Not SLParSplice, so always the "0th" trajectory
      iVisit++;
    }
    outTraj<<ss.str();
    outTraj.flush();

    officialTrajectory.clear();
    officialTrajectory.appendVisit(back);

    lastReport=std::chrono::high_resolution_clock::now();
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server reporting done ";
    std::cout<<"SPLICER DONE REPORTING "<<t.stop()<<std::endl;
  }
};

void ParSpliceSplicer::checkpoint(bool now){
  //checkpoint at given intervals
  if(now or std::chrono::high_resolution_clock::now() - lastCheckpoint> checkpointDelay  ) {
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server checkpointing ";
    //checkpoint();
    outTraj.close();
    outTime.close();

    //copy consistent progress files
    boost::filesystem::copy_file("./traj.out","./traj.out.chk",boost::filesystem::copy_option::overwrite_if_exists);
    boost::filesystem::copy_file("./times.out","./times.out.chk",boost::filesystem::copy_option::overwrite_if_exists);

    outTraj.open("./traj.out", std::ios::app | std::ios::out);
    outTime.open("./times.out", std::ios::app | std::ios::out);


    std::ofstream ofs("./Splicer.chk");
    // save data to archive
    {
      boost::archive::text_oarchive oa(ofs);
      // write class instance to archive
      oa << *this;
      // archive and stream closed when destructors are called
    }

    std::cout<<"CHECKPOINTING"<<std::endl;
    lastCheckpoint=std::chrono::high_resolution_clock::now();
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server checkpointing done";
  }
};


void ParSpliceSplicer::KMCSchedule( std::unordered_map<Label, unsigned > & visits){
  //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC ";


  //keep track of which segment in the database we already used
  std::unordered_map<Label, int> consumedSegments;

  //start at the current end
  Label c=officialTrajectory.back().label;
  double w=1;
  unsigned length=0;
  std::pair<Label, unsigned> s;
  {
    //boost::timer::auto_cpu_timer t(3, "KMC scheduling loop: %w seconds\n");

    while(true) {
      //"virtually" consume segment from the database
      auto key=std::make_pair(c,defaultFlavor);

      if(segmentDB.count(c,defaultFlavor)>0 and consumedSegments[c]< segmentDB.count(c,defaultFlavor) ) {
        Trajectory &t=segmentDB.db[key][consumedSegments[c]];
        consumedSegments[c]+=1;
        //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC virtual consume "<<c<<" "<<t.back().label;
        //std::cout<<"Splicer KMC virtual consume "<<c<<" "<<t.back().label<<std::endl;
        c=t.back().label;
      }
      //generate segment with BKL
      else{
        s=markovModel.sampleBKL(c, defaultVisitDuration, w);
        //std::cout<<"#Splicer KMC generate "<<c<<" "<<s.first<<" "<<s.second<<" "<<w<<std::endl;
        //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC generate "<<c<<" "<<s.first<<" "<<s.second<<" "<<w;
        int duration=s.second;
        Visit v;
        v.label=c;
        //do not exceed the prediction horizon
        v.duration=( duration+length>predictionTime ? predictionTime-length : duration );
        length+=v.duration;
        //visits.push_back(v);
        visits[v.label]+=v.duration; // RJZ: Should we be using += if v.label is not in visits yet??
        c=s.first;
        //break if weight becomes too low or we reached the prediction time
        //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC length "<<length<<" "<<v.duration<<" "<<s.second;
        if(length>=predictionTime or w<wMin) {
          break;
        }
      }
    }
  }
};

std::unordered_map<Label, int> ParSpliceSplicer::KMCScheduleAbs( std::unordered_map<Label, unsigned > & visits){
  //BOOST_LOG_SEV(lg,trace) <<"#Splicer KMC ";

  //list of segments we need
  std::unordered_map<Label, int> scheduledSegments;

  //start at the current end
  Label cStart=officialTrajectory.back().label;

  int nScheduled=0;

  double w=1;
  unsigned length=0;
  std::pair<Label, unsigned> s;
  while (nScheduled<predictionTime) {

    //Start a KMC simulation from cStart
    Label c=cStart;
    length=0;
    //boost::timer::auto_cpu_timer t(3, "KMC scheduling loop: %w seconds\n");


    //keep track of segments that were already used
    std::unordered_map<Label, int> consumedSegments;
    std::unordered_map<Label, int> consumedScheduledSegments;

    while(true) {
      //"virtually" consume segment from the database
      auto key=std::make_pair(c,defaultFlavor);

      //there is a corresponding segment in the database, consume it
      if(segmentDB.count(c,defaultFlavor)>0 and consumedSegments[c]< segmentDB.count(c,defaultFlavor) ) {
        Trajectory &t=segmentDB.db[key][consumedSegments[c]];
        consumedSegments[c]+=1;
        c=t.back().label;
      }
      //no corresponding segment in the database
      else{
        //sample end state with BKL
        s=markovModel.sampleBKL(c, defaultVisitDuration, w);

        //additional number of segments needed to cover this move
        int ns=s.second-(scheduledSegments.count(c)-consumedScheduledSegments[c]);


        //we have enough scheduled segments to cover this move, no need to schedule more
        if(ns<=0) {
          consumedScheduledSegments[s.first]+=s.second;
          c=s.first;
        }
        //we don't have enough segments to cover this move: schedule more segments and restart a KMC run
        else{
          scheduledSegments[c]+=ns;
          nScheduled+=ns;
          break;
        }
      }
    }
  }
  return scheduledSegments;
};


bool ParSpliceSplicer::spliceOne(){

  //BOOST_LOG_SEV(lg,trace) <<"#Splicer::splice ";
  //add segments to the official trajectory
  Label end=officialTrajectory.back().label;
  //BOOST_LOG_SEV(lg,info) <<"#Splicer Splice Current end: "<<end;
  auto key=std::make_pair(end,defaultFlavor);

  if( segmentDB.count(end,defaultFlavor)> 0 ) {

    Trajectory t;

    // Removed the entire trajectory at 'end'.. we can use everything
    segmentDB.front(end,defaultFlavor,t);
    segmentDB.pop_front(end,defaultFlavor);

    usedBlocks+=t.duration();
    //unusedBlocks-=t.size();
    officialTrajectory.splice(t);
    end=officialTrajectory.back().label;
    key=std::make_pair(end,defaultFlavor);
    //BOOST_LOG_SEV(lg,trace) <<"#Splicer Splice Current end: "<<end<<" "<<t.length;
    return true;
  }
  else{
    return false;
  }

};

#if 0
bool ParSpliceSplicer::splice(){
  //BOOST_LOG_SEV(lg,trace) <<"#Splicer::splice ";
  //add segments to the official trajectory
  Label end=officialTrajectory.back().label;
  //BOOST_LOG_SEV(lg,info) <<"#Splicer Splice Current end: "<<end;
  //std::cout<<"#Splicer Splice Current end: "<<end<<" "<<std::endl;
  Label originalEnd=end;
  auto key=std::make_pair(end,defaultFlavor);

  while( segmentDB.count(end,defaultFlavor)> 0 ) {
    //std::cout<<"#Splicer count: "<<segmentDB.count(end,defaultFlavor)<<std::endl;
    Trajectory t;
    segmentDB.front(end,defaultFlavor,t);
    segmentDB.pop_front(end,defaultFlavor);
    usedBlocks+=t.duration();
    //unusedBlocks-=t.size();
    officialTrajectory.splice(t);
    end=officialTrajectory.back().label;
    key=std::make_pair(end,defaultFlavor);
    //std::cout<<"#Splicer Splice Current end: "<<end<<" "<<std::endl;
    //BOOST_LOG_SEV(lg,trace) <<"#Splicer Splice Current end: "<<end<<" "<<t.length;
  }

  //officialTrajectory.print();
  return originalEnd != end;
};
#endif

void ParSpliceSplicer::output(){

  //output timings
  outTime<<std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-start).count()+carryOverTime<<" "<<usedBlocks<<" "<<producedBlocks<<std::endl;

  //output trajectory
  int iVisit=0;
  Visit back=officialTrajectory.back();
  officialTrajectory.pop_back();

  std::stringstream ss;
  for(auto it=officialTrajectory.visits.begin(); it!=officialTrajectory.visits.end(); it++) {
    ss<<" "<<it->label<<" "<<it->duration<<" 0\n"; // Not SLParSplice, so always the "0th" trajectory
    iVisit++;
  }
  outTraj<<ss.str();
  outTraj.flush();

  officialTrajectory.clear();
  officialTrajectory.appendVisit(back);

};






// Constructor:
SLParSpliceSplicer::SLParSpliceSplicer(MPI_Comm splicerComm_, AbstractDDS *minimaStore_, std::set<int> children_, boost::property_tree::ptree &config, MPI_Comm workerComm) :
  ParSpliceSplicer::ParSpliceSplicer(config) {

  splicerComm=splicerComm_;
  usedBlocks=unusedBlocks=wastedBlocks=producedBlocks=0;
  carryOverTime=0;
  minimaStore=minimaStore_;
  children=children_;
  validator.init(children,1,true); // <- sl=true

  start=std::chrono::high_resolution_clock::now();
  die=false;
  killed=false;

  //How many independent trajectories to splice:
  ntrajectories=config.get<unsigned>("ParSplice.MultiTrajectories.ntrajectories",NTRAJ_DEF);
  if(ntrajectories<1) ntrajectories=1;
  ncolors=config.get<unsigned>("ParSplice.MultiTrajectories.ncolors",NCOL_DEF);
  segblocks=config.get<unsigned>("ParSplice.MultiTrajectories.segblocks",SEGBLOCKS_DEF);
  useKmcPredict=config.get<unsigned>("ParSplice.MultiTrajectories.useKmcPredict",USEKMC_DEF);
  trajSLwait=config.get<unsigned>("ParSplice.MultiTrajectories.trajSLwait",TRAJSLWAIT_DEF);
  if(trajSLwait<1) trajSLwait=1;
  colorcnt=0;
  if((ntrajectories%ncolors)!=0) ncolors=1;

  //Check if Sub-lattice Decomposition will be used:
  useSubLattice=config.get<unsigned>("ParSplice.MultiTrajectories.useSubLattice",USESL_DEF);
  if(useSubLattice) {
    std::cout<<"Using Sub-lattice Decomposition! (Experimental Version)"<<std::endl;
    check1st=config.get<unsigned>("ParSplice.MultiTrajectories.check1st",CHECK1ST_DEF);
    nxyzSL.push_back(config.get<int>("ParSplice.MultiTrajectories.nxSL",NSL_DEF));
    nxyzSL.push_back(config.get<int>("ParSplice.MultiTrajectories.nySL",NSL_DEF));
    nxyzSL.push_back(config.get<int>("ParSplice.MultiTrajectories.nzSL",NSL_DEF));
    std::vector<int> ncxyz = {1, 1, 1};
    for(int i=0; i<3; i++) {
      if(nxyzSL[i]%2 != 0) nxyzSL[i]=(nxyzSL[i]/2)*2;
      if(nxyzSL[i]<1) nxyzSL[i]=1;
      if(nxyzSL[i]>1) ncxyz[i] =2;
    }
    ncolors = ncxyz[0]*ncxyz[1]*ncxyz[2];
    for(int i=0; i<ncolors; i++) colorMap[i] = {};
    ntrajectories = 0;
    ndomains = 0;
    for(int kk=0; kk<nxyzSL[2]; kk++) {
      for(int jj=0; jj<nxyzSL[1]; jj++) {
        for(int ii=0; ii<nxyzSL[0]; ii++) {
          int thiscolor = (kk%ncxyz[2])*ncxyz[2]*ncxyz[1] +
                          (jj%ncxyz[1])*ncxyz[1] +
                          (ii%ncxyz[0]);
          SLDTrajectory.push_back(Trajectory());
          SLDTrajectory[ntrajectories].color = thiscolor;
          colorMap[thiscolor].push_back(ntrajectories);
          std::vector<int> xyzSL = {ii,jj,kk};
          officialSyncStatus.push_back(SyncStatus(ntrajectories,nxyzSL,xyzSL));
          sldlabels.push_back(0);
          sldtrans.push_back(0);
          ntrajectories++;
          // If thiscolor is the maximum color index,
          // We have defined a full 'domain'...
          if(thiscolor==(ncolors-1)){
            std::vector <unsigned> subdomains = {};
            int zzi=kk; int yyi=jj; int xxi=ii;
            if(nxyzSL[2]>1) zzi--;
            if(nxyzSL[1]>1) yyi--;
            if(nxyzSL[0]>1) xxi--;
            for(int zz=zzi; zz<(kk+1); zz++){
              for(int yy=yyi; yy<(jj+1); yy++){
                for(int xx=xxi; xx<(ii+1); xx++){
                  int itraj = (zz)*nxyzSL[1]*nxyzSL[0] +
                              (yy)*nxyzSL[0] +
                              (xx);
                  subdomains.push_back(itraj);
                  SLDTrajectory[itraj].domainId = ndomains;
                }
              }
              domainList.push_back(Domain());
              domainList[ ndomains ].trajectories = subdomains;
              domainList[ ndomains ].nKMCPred = 0;

              if(outputvol > 1){
                std::cout<<" DOMAIN "<<ndomains<<":";
                for(int j=0; j<ncolors; j++){
                  std::cout<<" "<<domainList[ ndomains ].trajectories[j];
                }
                std::cout<<std::endl;
              }

              ndomains++;
            }
          }
        }
      }
    }
  }else{
    ndomains = 0;
    std::vector <unsigned> subdomains = {};
    int dcnt=0;
    for(int i=0; i<ntrajectories; i++){
      subdomains.push_back(i); dcnt++;
      SLDTrajectory[i].domainId = ndomains;
      if(dcnt == ncolors){
         domainList.push_back(Domain());
         domainList[ ndomains ].trajectories = subdomains;
         domainList[ ndomains ].nKMCPred = 0;
         ndomains++;
         subdomains = {};
         dcnt=0;
      }
    }
    for(int i=0; i<ncolors; i++) colorMap[i] = {};
  }
  if(outputvol > 1){
    std::cout<<"Using ntrajectories = "<<ntrajectories
             <<" and ncolors = "<<ncolors<<std::endl;
  }

  for(int i=0; i<ntrajectories; i++) {
    usedBlocksSD.push_back(0);
    if(!useSubLattice) {
      SLDTrajectory.push_back(Trajectory());
      SLDTrajectory[i].color = i % ncolors;
      colorMap[i % ncolors].push_back(i);
      officialSyncStatus.push_back(SyncStatus(i,{1,1,1},{0,0,0}));
      sldlabels.push_back(0);
      sldtrans.push_back(0);
    }
    SLDTrajectory[i].blocksThisCycle = 0;
    SLDTrajectory[i].nKMCPred = 0;
  }

  //check if checkpoint files are present
  bool restartFromCheckPoint= boost::filesystem::exists("./traj.out.chk") && boost::filesystem::exists("./times.out.chk") && boost::filesystem::exists("./Splicer.chk");
  if(restartFromCheckPoint) {
    boost::filesystem::copy_file("./traj.out.chk","./traj.out",boost::filesystem::copy_option::overwrite_if_exists);
    boost::filesystem::copy_file("./times.out.chk","./times.out",boost::filesystem::copy_option::overwrite_if_exists);
    outTraj.open("./traj.out", std::ios::app);
    outTime.open("./times.out", std::ios::app);
  }
  else{
    outTraj.open("./traj.out", std::ios::out);
    outTime.open("./times.out", std::ios::out);
  }
  if(useSubLattice){
    // TODO: Add a trajSL.out.chk etc..
    outTrajSL.open("./trajSL.out", std::ios::out);
  }

  ovrdDelay = 0;
  currentcolor = 0;
  minPerCycle = config.get<unsigned>("ParSplice.MultiTrajectories.minPerCycle",SLCYCLE_DEF);
  if(minPerCycle<1) minPerCycle=1;

  //initialize from config data
  wMin=config.get<double>("ParSplice.MC.MinimumTrajectoryWeigth",WMIN_DEF);
  defaultVisitDuration=config.get<unsigned>("ParSplice.MC.DefaultVisitDuration",VISDUR_DEF);
  predictionGranularity=config.get<unsigned>("ParSplice.MC.PredictionGranularity",PREDGRAN_DEF);
  batchSize=config.get<unsigned>("ParSplice.Topology.NWorkers",NWORKER_DEF);
  predictionTime=config.get<unsigned>("ParSplice.Topology.NWorkers",NWORKER_DEF);
  maxTaskMesgSize=config.get<int>("ParSplice.MaximumTaskMesgSize",MXTASKMSG_DEF);
  validatorTimeout=config.get<unsigned>("ParSplice.Splicer.ValidatorTimeout",VALTOUT_DEF);
  // Should always include the current head of ALL trajectories being spliced.
  // So... Ensure that: batchSize >= ntrajectories
  // (For SL, may only need to include the head of all trajectories for the
  // current "color" ??)
  if(batchSize < (ntrajectories / ncolors))
    batchSize = (ntrajectories / ncolors);
  // Dont try to predict too far into the future if you are using multiple
  // trajectories...
  //predictionTime is the prediction horizon for EACH domain:
  predictionTime = segblocks * predictionTime / (ntrajectories / ncolors);
  if(predictionTime < segblocks) predictionTime = segblocks;
  broadcastDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.BroadcastDelay",BCASTDELAY_DEF) );
  reportDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.ReportDelay",RPRTDELAY_DEF) );
  checkpointDelay=std::chrono::milliseconds( config.get<unsigned>("ParSplice.Splicer.CheckpointDelay",CHKDELAY_DEF) );
  flushOnModify= config.get<bool>("ParSplice.Splicer.FlushOnModify",FLUSHONMOD_DEF);
  runTime=std::chrono::minutes( config.get<unsigned>("ParSplice.RunTime",RUNTIME_DEF) );

  //read task parameters
  BOOST_FOREACH(boost::property_tree::ptree::value_type &v, config.get_child("ParSplice.TaskParameters")) {
    std::string stype=v.second.get<std::string>("Type");
    boost::trim(stype);
    int type=taskTypeByLabel.at(stype);
    int flavor=v.second.get<int>("Flavor");
    BOOST_FOREACH(boost::property_tree::ptree::value_type &vv, v.second.get_child("")) {
      std::string key=vv.first;
      std::string data=vv.second.data();
      boost::trim(key);
      boost::trim(data);
      std::cout<<key<<" "<<data<<std::endl;
      taskParameters[std::make_pair(type,flavor)][key]=data;
    }
  }

  driver=new DriverHandleType(workerComm);

  //TODO: FIX ME
  defaultFlavor=1;
  STATE_DBKEY=0;
  batchId=1;
  broadcastRequests=std::vector<MPI_Request>(children.size(),MPI_REQUEST_NULL);

  if(restartFromCheckPoint) {
    // create and open an archive for input
    std::ifstream ifs("Splicer.chk");
    boost::archive::text_iarchive ia(ifs);
    // read class state from archive
    ia >> *this;
    // archive and stream closed when destructors are called
  }
  else{
    for(int itrajectory = 0; itrajectory<ntrajectories; itrajectory++) {
      addsystem_byfile(config, itrajectory);
    }
  }

  for(int i=0; i<ntrajectories; i++) {
    if(outputvol > 1){
      std::cout<<"INITIAL TRAJECTORY "<<i<<": "<<std::endl;
      SLDTrajectory[i].print();
    }
    sldlabels[i] = SLDTrajectory[i].back().label;
  }

};

SLParSpliceSplicer::~SLParSpliceSplicer(){
  delete driver;
};

void SLParSpliceSplicer::addsystem_byfile(boost::property_tree::ptree &config, int itrajectory){

  TaskType task;

  if(useSubLattice) {
    std::vector<std::string> sldfile_list;
    std::string initialConfiguration=config.get<std::string>("ParSplice.InitialConfiguration");
    boost::trim(initialConfiguration);
    sldfile_list.push_back(initialConfiguration);
    sldfile_list.push_back(boost::lexical_cast<std::string>(itrajectory));
    std::string initialConfigurationSLD = boost::algorithm::join(sldfile_list, ".");

    task.type=PARSPLICE_TASK_INIT_FROM_FILE;
    task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_INIT_FROM_FILE,defaultFlavor)];
    task.parameters["Filename"]=initialConfigurationSLD;
    driver->assign(task);
    while(not driver->probe(task)) {};
    task.parameters.clear();

  }else{
    std::string initialConfiguration=config.get<std::string>("ParSplice.InitialConfiguration");
    boost::trim(initialConfiguration);

    task.type=PARSPLICE_TASK_INIT_FROM_FILE;
    task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_INIT_FROM_FILE,defaultFlavor)];
    task.parameters["Filename"]=initialConfiguration;
    driver->assign(task);
    while(not driver->probe(task)) {};
    task.parameters.clear();
  }

  if(useSubLattice){
    task.type=PARSPLICE_TASK_MOVE_FIRST;
    task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MOVE_FIRST,defaultFlavor)];
    driver->assign(task);
    while(not driver->probe(task)) {};
  }

  task.type=PARSPLICE_TASK_MIN;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MIN,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};
  task.systems[0].type=TYPE::MINIMUM;

  task.type=PARSPLICE_TASK_LABEL;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_LABEL,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};

  task.type=PARSPLICE_TASK_REMAP;
  task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_REMAP,defaultFlavor)];
  driver->assign(task);
  while(not driver->probe(task)) {};

  Rd data;
  pack(data,task.systems[0]);
  minimaStore->put(STATE_DBKEY,task.systems[0].label,data);
  if(outputvol > 1)
    std::cout<<"DB STORE "<<data.size()<<" "<<task.systems[0].label<<std::endl;

  Visit v;
  v.label=task.systems[0].label;
  v.duration=0;
  SLDTrajectory[itrajectory].appendVisit(v);
  SLDTrajectory[itrajectory].flavor=defaultFlavor;

};

void SLParSpliceSplicer::processRecv(){
  //did we receive new completed tasks?

  if(die) {
    std::cout<<"Splicer: cancelling pending receives"<<std::endl;
    MPI_Cancel(&incomingRequest);
    MPI_Wait(&incomingRequest,&recvStatus);
  }
  else{

    if(not recvCompleted) {
      MPI_Test(&incomingRequest,&recvCompleted,&recvStatus);
    }
    if(recvCompleted) {
      Timer t;
      //extract
      int peer=recvStatus.MPI_SOURCE;
      int tag=recvStatus.MPI_TAG;
      int count=0;
      MPI_Get_count( &recvStatus, MPI_BYTE, &count );

      switch (tag) {
        case PARSPLICE_STATS_SEGMENTS_TAG: {

          std::list<SegmentDatabase> incomingBatches;
          TransitionStatistics statistics;
          unpack(rbuf,statistics,incomingBatches,std::size_t(count));

          for(auto itbatch=incomingBatches.begin(); itbatch!=incomingBatches.end(); itbatch++) {
            for(auto itdb=itbatch->db.begin(); itdb!=itbatch->db.end(); itdb++) {
              for(auto ittraj=itdb->second.begin(); ittraj!=itdb->second.end(); ittraj++) {
                producedBlocks+=ittraj->duration();
              }
            }
          }

          //validate the new batches
          for(auto it=incomingBatches.begin(); it!=incomingBatches.end(); it++) {
            //std::cout<<"SPLICER VALIDATED SEGMENTS "<<std::endl;
            //it->print();
            validator.validate(peer,*it);
          }

          //transfer to segmentDB
          while(true) {
            validator.refreshHead(validatorTimeout);
            SegmentDatabase validatedSegments=validator.release();
            if(validatedSegments.size()>0) {
              //std::cout<<"SPLICER MERGING SEGMENTS "<<std::endl;
              //validatedSegments.print();
              bool sl=true;
              segmentDB.merge(validatedSegments,sl);
            }
            else{
              break;
            }
          }

          //update Markov model
          markovModel.assimilate(statistics);
          break;
        }

        case PARSPLICE_SYNC_STATS_TAG: {

          if(outputvol > 2) std::cout<<"SPLICER RECVING SYNC RESULT "<<std::endl;

          SyncStatistics syncstatistics;
          unpack(rbuf,syncstatistics);

          if(outputvol > 2) std::cout<<"SPLICER UNPACKED SYNC RESULT "<<std::endl;

          // DO SOMETHING HERE WITH THE SYNC STATISTICS!!
          // (i.e. update status of the "waiting" trajectories)
          std::map<unsigned, SyncData> syncMap = syncstatistics.syncMap;
          for(auto it=syncMap.begin(); it!=syncMap.end(); it++) {

            if(outputvol > 2) std::cout<<"SPLICER ITERATING IN SYNC RESULT "<<std::endl;

            unsigned itrajectory = it->first;
            SyncData sd = it->second;

            if(outputvol > 2){
              std::cout<<"SPLICER  itrajectory = "<<itrajectory<<std::endl;
              std::cout<<"SPLICER  SyncData.batchId = "<<sd.batchId<<std::endl;
              std::cout<<"SPLICER  SyncData.label = "<<sd.label<<std::endl;
            }

            if(officialSyncStatus[itrajectory].batchId==sd.batchId) {
              Label oldlabel = SLDTrajectory[itrajectory].back().label;
              Label newlabel = sd.label;
              if(outputvol > 1){
                std::cout<<"SYNC "<<sd.batchId<<" of itrajectory "<<itrajectory
                <<" -> old label = "<<oldlabel
                <<" -> new label = "<<newlabel<<std::endl;
              }

              // Update sldlabels if there was a sync:
              sldlabels[itrajectory] = newlabel;

              //TODO: Check if it is this simple:
              Visit v;
              v.label=sd.label;
              v.duration=0;
              SLDTrajectory[itrajectory].appendVisit(v);
              //SLDTrajectory[itrajectory].back().label=sd.label;

              officialSyncStatus[itrajectory].clear();
              if(oldlabel!=newlabel) {
                // Clear domain predictions...
                int idomain = SLDTrajectory[itrajectory].domainId;
                domainList[idomain].predm.clear();
                domainList[idomain].nKMCPred=0;
              }

              //if(SLDTrajectory[itrajectory].color==currentcolor){
                ovrdDelay=1;
                processSend();
              //}

            }else if(outputvol > 2){
              std::cout<<"Sync already done for itrajectory "<<itrajectory
              <<" and batchId "<<sd.batchId<<std::endl;
            }
          }
          if(outputvol > 2) std::cout<<"SPLICER DONE PROCESSING SYNC RESULT."<<std::endl;
          break;
        }

        default: {
          //We don't support any other task yet
          assert(tag==PARSPLICE_STATS_SEGMENTS_TAG or tag==PARSPLICE_SYNC_STATS_TAG);
          break;
        }

      }

      //post a new receive for tasks
      MPI_Irecv(&(rbuf[0]),rbufSize,MPI_BYTE,MPI_ANY_SOURCE,MPI_ANY_TAG,splicerComm,&incomingRequest);
      recvCompleted=0;

    }
  }
};

void SLParSpliceSplicer::sendSyncBatch(std::set<unsigned> syncset){

  Timer t;
  //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server broadcasting ";
  if(outputvol > 2) std::cout<<"SPLICER BROADCASTING BCAST BATCH"<<std::endl;
  //std::cout<<"CURRENT HEAD(S): "<<std::endl;
  //for(int i=0; i<ntrajectories; i++) {
  //  std::cout<<SLDTrajectory[i].back().label <<std::endl;
  //}

  //reset the send buffer
  std::vector<std::vector<char> > sBuf(children.size());

  int nTask = syncset.size();

  //split the load between children
  //TODO: This has to be improved to consider affinity
  unsigned nTaskPerChild=(children.size()>0 ? nTask/children.size() : 0);
  unsigned np=nTask%children.size();

  //pack and send the messages
  int iChild=0;
  for(auto it=children.begin(); it!=children.end(); it++,iChild++) {

    //create the task bundle
    TaskDescriptorBundle taskBundle;
    TaskDescriptor task;
    task.type=PARSPLICE_TASK_SYNC_SL;
    task.flavor=defaultFlavor;
    task.batchId=batchId;
    unsigned n=0;
    unsigned nt=nTaskPerChild+(iChild<np ? 1 : 0);

    for(auto its=syncset.begin(); its!=syncset.end(); its++) {

      Label lb = SLDTrajectory[*its].back().label;
      if(outputvol > 1)
        std::cout<<"SCHEDULING SYNC OF STATE " <<lb<<" FOR WORKER "<<*it<<std::endl;

      task.nInstances=1;
      task.itrajectory=*its;
      task.systems.clear();
      SystemPlaceholder s;
      s.label=lb;         // Original state label for the SLD (trajectory) [to be the 0th system in the task]
      s.necessity=NECESSITY::REQUIRED;
      s.type=TYPE::MINIMUM;
      task.systems.push_back(s);

      // Now we need to add the systems that make up the "changed" neighbors:
      SyncStatus oSs = officialSyncStatus[*its];
      for(auto itc=oSs.changelist.begin(); itc!=oSs.changelist.end(); itc++) {
        // Note: "*itc" is a relation vector (ex. (1, 0 , -1)) of the
        // trajectory's neighbor, and "neighlabels" translates this vector to
        // the new state 'label' that the neighbor has transitioned to
        // .. "neighlabelsI" is the initial label for the neighbor

        if(check1st){
          // Original label of neighbor 1st
          s.label = oSs.neighlabelsI[*itc];
          s.necessity=NECESSITY::REQUIRED;
          s.type=TYPE::MINIMUM;
          task.systems.push_back(s);
        }

        // Changed label of neighbor 2nd
        s.label = oSs.neighlabels[*itc];
        s.necessity=NECESSITY::REQUIRED;
        s.type=TYPE::MINIMUM;
        task.systems.push_back(s);
      }
      // Should also package simple info about the neighbors:
      task.changelist = oSs.changelist;
      taskBundle.insert(task);
    }
    pack(sBuf[iChild],taskBundle);
    //std::cout<<splicerComm<<" "<<&(sBuf[iChild][0])<<" "<<sBuf[iChild].size()<<" "<<*it<<" "<<PARSPLICE_TASK_TAG <<" "<< &(broadcastRequests[iChild])<<std::endl;

    //TODO make this more robust
    assert(sBuf[iChild].size()<maxTaskMesgSize);
    MPI_Issend(&(sBuf[iChild][0]),sBuf[iChild].size(),MPI_BYTE,*it,PARSPLICE_TASK_TAG,splicerComm, &(broadcastRequests[iChild]));

    break; // Only send the synchronization task to the 0th child (for now)
  }

  lastBroadcast=std::chrono::high_resolution_clock::now();
  if(outputvol > 2) std::cout<<"SPLICER DONE BROADCASTING SYNC BATCH "<<t.stop()<<std::endl;
  batchId++;
};

void SLParSpliceSplicer::processSend(){

  //test for completion of the previous task broadcast
  MPI_Testall(children.size(), &(broadcastRequests[0]),&broadcastCompleted, MPI_STATUSES_IGNORE);

  if(die and not killed) {
    std::cout<<"Splicer: killing the workers"<<std::endl;
    MPI_Waitall(children.size(), &(broadcastRequests[0]),MPI_STATUSES_IGNORE);

    TaskDescriptorBundle taskBundle;
    TaskDescriptor task;
    task.type=PARSPLICE_TASK_DIE;
    task.flavor=defaultFlavor;
    task.batchId=batchId;
    task.nInstances=1;
    task.systems.clear();
    taskBundle.insert(task);

    int iChild=0;
    for(auto it=children.begin(); it!=children.end(); it++,iChild++) {
      pack(sBuf[iChild],taskBundle);
      //TODO make this more robust
      assert(sBuf[iChild].size()<maxTaskMesgSize);
      MPI_Issend(&(sBuf[iChild][0]),sBuf[iChild].size(),MPI_BYTE,*it,PARSPLICE_TASK_TAG,splicerComm, &(broadcastRequests[iChild]));
    }
    killed=true;

    MPI_Waitall(children.size(), &(broadcastRequests[0]),MPI_STATUSES_IGNORE);
  }

  //buffer the predictions and broadcast only if delay has passed since the last time
  //also make sure we are done broadcasting the previous batch
  if((std::chrono::high_resolution_clock::now() - lastBroadcast> broadcastDelay and broadcastCompleted)
                                                                || (ovrdDelay and broadcastCompleted)) {

    if(ovrdDelay==1) ovrdDelay=0;

    //rjz// First, check if we have any syncronization tasks to do:
    std::set<unsigned> syncset;
    if(useSubLattice && (ntrajectories>1)) {
      for(int i=0; i<ntrajectories; i++) {
        //if(SLDTrajectory[i].color!=currentcolor) continue;
        if((officialSyncStatus[i].length>0)&&(officialSyncStatus[i].batchId==0)) {
          syncset.insert(i);
          officialSyncStatus[i].batchId=batchId;
          //std::cout<<"NEED SYNC: itrajectory = "<<i
          //<<" - batchId = "<<batchId<<std::endl;
        }
      }
      if(syncset.size()>0) {
        sendSyncBatch(syncset);
        return;
        // Don't change 'lastBroadcast' so other trajectories can be assigned
        // (traj's that dont need to be synced)?
      }
    }

    Timer t;
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server broadcasting ";
    if(outputvol > 1){
      std::cout<<"SPLICER BROADCASTING"<<std::endl;
      std::cout<<"CURRENT HEAD(S): "<<std::endl;
      for(int i=0; i<ntrajectories; i++) {
        if(SLDTrajectory[i].color!=currentcolor) continue;
        std::cout<<SLDTrajectory[i].back().label<<std::endl;
      }
    }

    //reset the send buffer
    sBuf=std::vector<std::vector<char> >(children.size());

    {

      //TODO: make this faster
      std::map<Label, unsigned > counts;
      int nKeep=0;

      // NEW EXPERIMENTAL (YES-KMC)
      if(1){

        // Keep track of which segments in the database are accounted for
        // (when a non-KMC prediction is used)
        std::unordered_map<Label, int> consumedSegments;

        // Loop trough all domains in the system
        for(int idomain=0; idomain<ndomains; idomain++){

          int nKeep_d=0;

          // Check if the head of this domain is in predm,
          // (NOTE: Might be a bad idea if we are at MinPerCycle already!)
          // and that we have performed at least 10 kmc iterations...

          unsigned trajcheck = domainList[idomain].trajectories[currentcolor];
          //itt = domainList[idomain].predm.find(trajcheck.back().label);
          //if((itt != domainList[idomain].predm.end())
          //     && (domainList[idomain].nKMCPred >= 10)){
          if(useKmcPredict && domainList[idomain].nKMCPred >= 10 && domainList[idomain].predm.size()>0){

            // Okay - We have a good kmc-based prediction for this domain, use it...

            std::set<SchedulerState> schedulingSet;
            for(auto it=domainList[idomain].predm.begin(); it!=domainList[idomain].predm.end(); it++) {
              it->second.erase(0);
            }
            for(auto it=domainList[idomain].predm.begin(); it!=domainList[idomain].predm.end(); it++) {
              SchedulerState s;
              s.label=it->first;
              s.counts=it->second;
              if(s.counts.size()>0) schedulingSet.insert(s);
            }

            // Need the KMC preditictions to produce enough 'counts'.
            // So each time an 's.counts' object is removed from 'schedulingSet',
            // it should correspond to 'asgnedGranularity' blocks
            // (which means 'countsGranularity' is added to 'counts' for that label)

            // define counts to store "floats"
            std::map<Label, float > fcounts;

            float asgned=0;
            while(((int) asgned)<=predictionTime && schedulingSet.size()>0) {
              //find the state with the largest number of KMC instances that required more segments
              auto it=schedulingSet.rbegin();
              SchedulerState s=*it;
              //remove from the set
              schedulingSet.erase(std::next(it).base());

              auto its = s.counts.begin();
              float countsGranularity = (((float) *its) / ((float) segblocks))
                                      / ((float) domainList[idomain].nKMCPred);
              float asgnedGranularity = countsGranularity * segblocks;

              asgned+=asgnedGranularity;
              fcounts[s.label]+=countsGranularity;
              s.counts.erase(its);

              //reinsert if there are KMC instances left
              if(s.counts.size()>0) {
                //hint that the updated state might still be close to the back
                schedulingSet.insert(schedulingSet.cend(),s);
              }
            }

            for(auto it=fcounts.begin(); it!=fcounts.end(); it++) {
              int cntadd = (int) (it->second);
              if(cntadd < 1) cntadd = 1;
              nKeep+=cntadd;
              counts[it->first]+=cntadd;
            }

            //clear the previous predictions
            domainList[idomain].predm.clear();
            domainList[idomain].nKMCPred=0;

          }else{

            // We DON'T have a good kmc-based prediction,
            // Just put segments in the current states of this domain...
            // (Also, use existing segments in DB to improve the prediction)

            int asgned=0;
            for(int icolor=0; icolor<ncolors; icolor++){
              int color = (currentcolor + icolor) % ncolors;
              unsigned i = domainList[idomain].trajectories[color];
              asgned+=SLDTrajectory[i].blocksThisCycle;
              if(useSubLattice && (officialSyncStatus[i].length > 0)) break;
              Label lbl = SLDTrajectory[i].back().label;
              while((asgned < minPerCycle*(icolor+1)) && (nKeep_d < batchSize/ndomains)){
                //"virtually" consume segment from the database (if its there)
                auto key=std::make_pair(lbl,defaultFlavor);
                if(segmentDB.count(lbl,defaultFlavor)>0 and consumedSegments[lbl]< segmentDB.count(lbl,defaultFlavor) ) {
                  Trajectory &t=segmentDB.db[key][consumedSegments[lbl]];
                  consumedSegments[lbl]+=1;
                  lbl=t.back().label;
                  asgned+=segblocks;
                }else{
                  // No segment in DB.. Lets assign one:
                  counts[lbl]+=1;
                  nKeep_d+=1;
                  nKeep+=1;
                  asgned+=segblocks; // We have actually assigned 'segblocks' blocks with each segment
                }
              }
            }

          }

        }

      // BASIC #1 (NO-KMC)... Only assign segments in current color
      }else if(1){

        std::vector<unsigned> asgned;
        for(int i=0; i<ntrajectories; i++){
          asgned.push_back(SLDTrajectory[i].blocksThisCycle);
        }
        while(nKeep<batchSize){
          int nskip = 0;
          for(int i=0; i<ntrajectories; i++){
            if(SLDTrajectory[i].color != currentcolor) continue;
            if(asgned[i] >= minPerCycle){
              nskip++;
              continue;
            }
            Label lbl = SLDTrajectory[i].back().label;
            counts[lbl]+=1;
            nKeep+=1;
            asgned[i]+=segblocks; // We have actually assigned 'segblocks' blocks with each segment
          }
          if(nskip>=(ntrajectories/ncolors)) break;
        }
        // Debug option to print the schedulingSet information
        if(outputvol > 1) std::cout<<" schedulingSet ----> "<<std::endl<<std::flush;
        for(auto it=counts.begin(); it!=counts.end(); it++) {
          it->second = (int) (it->second);
          if(it->second < 1) it->second=1;
          if(outputvol > 1)
            std::cout<<it->first<<" "<<it->second<<std::endl<<std::flush;
        }

      // BASIC #2 (NO-KMC)... Try to assign segments in head of all domains
      // (current color and future colors)
      }else{

        std::vector<unsigned> asgned;
        for(int i=0; i<ntrajectories; i++){
          asgned.push_back(SLDTrajectory[i].blocksThisCycle);
        }
        int cloop=1;
        while(nKeep<batchSize){
          for(unsigned icolor=0; icolor<ncolors; icolor++){
            if(nKeep>=batchSize) break;
            unsigned tcolor = (currentcolor+icolor) % ncolors;
            std::vector <unsigned> trajList = colorMap[tcolor];
            while(nKeep<batchSize){
              int nskip = 0;
              for(int iSLD=0; iSLD<trajList.size(); iSLD++) {
                int i = trajList[iSLD];
                if(asgned[i] >= minPerCycle*cloop){
                  nskip++; continue;
                }
                if(useSubLattice && (officialSyncStatus[i].length > 0)){
                  nskip++; continue;
                }
                Label lbl = SLDTrajectory[i].back().label;
                counts[lbl]+=1;
                nKeep+=1;
                asgned[i]+=segblocks;
                if(nKeep>=batchSize) break;
              }
              if(nskip>=(ntrajectories/ncolors)) break;
            }
          }
          cloop++;
        }
        // Debug option to print the schedulingSet information
        //std::cout<<" schedulingSet ----> "<<std::endl<<std::flush;
        for(auto it=counts.begin(); it!=counts.end(); it++) {
          it->second = (int) (it->second);
          if(it->second < 1) it->second=1;
          //std::cout<<it->first<<" "<<it->second<<std::endl<<std::flush;
        }

      }

      //split the load between children
      //TODO: This has to be improved to consider affinity
      unsigned nTaskPerChild=(children.size()>0 ? nKeep/children.size() : 0);
      unsigned np=nKeep%children.size();

      //pack and send the messages
      int iChild=0;
      for(auto it=children.begin(); it!=children.end(); it++,iChild++) {

        //create the task bundle
        TaskDescriptorBundle taskBundle;
        TaskDescriptor task;
        task.type=PARSPLICE_TASK_SEGMENT;
        task.flavor=defaultFlavor;
        task.batchId=batchId;
        unsigned n=0;
        unsigned nt=nTaskPerChild+(iChild<np ? 1 : 0);
        for(auto itc=counts.begin(); itc!=counts.end(); ) {
          unsigned nInstances=itc->second;
          Label lb=itc->first;

          //we can take in all of these tasks
          if(n+nInstances<nt) {
            n+=nInstances;
            itc=counts.erase(itc);
          }
          //we can only take part of these tasks
          else{
            nInstances=nt-n;
            n=nt;
            itc->second-=nInstances;
          }
          if(outputvol > 1)
            std::cout<<"SCHEDULING "<<nInstances<<" INSTANCES IN STATE " <<lb<<" FOR WORKER "<<*it<<std::endl;

          task.nInstances=nInstances;
          task.systems.clear();
          SystemPlaceholder s;
          s.label=lb;
          s.necessity=NECESSITY::REQUIRED;
          s.type=TYPE::MINIMUM;
          task.systems.push_back(s);
          s.necessity=NECESSITY::OPTIONAL;
          s.type=TYPE::QSD;
          task.systems.push_back(s);

          taskBundle.insert(task);

          if(n==nt) {
            break;
          }
        }
        pack(sBuf[iChild],taskBundle);

        //TODO make this more robust
        assert(sBuf[iChild].size()<maxTaskMesgSize);
        MPI_Issend(&(sBuf[iChild][0]),sBuf[iChild].size(),MPI_BYTE,*it,PARSPLICE_TASK_TAG,splicerComm, &(broadcastRequests[iChild]));
      }
    }
    lastBroadcast=std::chrono::high_resolution_clock::now();

    if(outputvol > 1)
      std::cout<<"SPLICER DONE BROADCASTING "<<t.stop()<<std::endl;
    batchId++;
  }
};


void SLParSpliceSplicer::spliceSLD(int itrajectory){
  Timer t;
  unsigned long usedBefore;

  // Check for early return:
  if(SLDTrajectory[itrajectory].blocksThisCycle >= minPerCycle) return;

  Label previousLabel=SLDTrajectory[itrajectory].back().label;

  bool spliced=false;
  while(usedBefore=usedBlocksSD[itrajectory], spliceOne(itrajectory)) {
    spliced=true;
    if(modifier.modificationNeeded(usedBefore,usedBlocksSD[itrajectory]))
    {
      if(outputvol > 2)
        std::cout<<"MODIFY "<<SLDTrajectory[itrajectory].back().label<<std::endl;
      Rd data;
      SystemType s;
      uint64_t res=minimaStore->reserve(STATE_DBKEY,SLDTrajectory[itrajectory].back().label);
      while(not minimaStore->get(STATE_DBKEY, SLDTrajectory[itrajectory].back().label,data)) {minimaStore->singleServe();}
      minimaStore->release(res);
      unpack(data,s,0);

      modifier.modify(s, usedBefore, usedBlocksSD[itrajectory]);

      if(outputvol > 2) std::cout<<"MODIFY DONE"<<std::endl;
      TaskType task;
      task.systems.push_back(s);

      task.type=PARSPLICE_TASK_MIN;
      task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_MIN,defaultFlavor)];
      driver->assign(task);
      while(not driver->probe(task)) {};
      task.systems[0].type=TYPE::MINIMUM;
      if(outputvol > 2) std::cout<<"MIN DONE"<<std::endl;

      task.type=PARSPLICE_TASK_REMAP;
      task.parameters=taskParameters[std::make_pair(PARSPLICE_TASK_REMAP,defaultFlavor)];
      driver->assign(task);
      while(not driver->probe(task)) {};
      task.systems[0].type=TYPE::MINIMUM;
      if(outputvol > 2) std::cout<<"REMAP DONE "<<task.systems[0].label<<std::endl;

      data.clear();
      pack(data,task.systems[0]);
      minimaStore->put(STATE_DBKEY,task.systems[0].label,data);
      if(outputvol > 2) std::cout<<"PUT DONE"<<std::endl;

      Visit v;
      v.label=task.systems[0].label;
      v.duration=0;
      SLDTrajectory[itrajectory].appendVisit(v);
      if(outputvol > 2) std::cout<<"APPEND DONE"<<std::endl;

      if(flushOnModify) {
        markovModel.clear();
        segmentDB.clear();
      }
    }
    if((ncolors>1) &&
      (SLDTrajectory[itrajectory].blocksThisCycle >= minPerCycle)){

      // Clear domain predictions...
      int idomain = SLDTrajectory[itrajectory].domainId;
      domainList[idomain].predm.clear();
      domainList[idomain].nKMCPred=0;

      break;
    }
  }
  Label currentLabel=SLDTrajectory[itrajectory].back().label;
  if(currentLabel!=previousLabel) {

    // Add synchronization item when color is finished...
    sldtrans[itrajectory]=1;

    // Clear domain predictions...
    int idomain = SLDTrajectory[itrajectory].domainId;
    domainList[idomain].predm.clear();
    domainList[idomain].nKMCPred=0;

  }

};

void SLParSpliceSplicer::splice(){

  bool colordone = 1;
  bool needsync = 0;

  // Loop until we have not finished a color,
  // or if we need to synchronize someone:
  while(colordone && (!needsync)){

    // Loop through the trajectories that belong to currentcolor:
    std::vector <unsigned> trajList = colorMap[currentcolor];
    for(int icolor=0; icolor<trajList.size(); icolor++) {
      int i = trajList[icolor];
      if(useSubLattice && (officialSyncStatus[i].length > 0)){
        // This trajectory needs to be synchronized...
        colordone = 0; continue;
      }
      // Splice this trajectory until minPerCycle,
      // or until out of spliceable segments:
      spliceSLD(i);
      if(SLDTrajectory[i].blocksThisCycle < minPerCycle) colordone = 0;
    }

    if(colordone) {

      for(int icolor=0; icolor<trajList.size(); icolor++) {
        SLDTrajectory[ trajList[icolor] ].blocksThisCycle -= minPerCycle;
      }

      // Write trajSL.out line if colorcnt==0
      colorcnt=(colorcnt+1)%trajSLwait;

      // Make sure all previous sync tasks are complete...
      // (Only current color is guarenteed to be done)
      // THIS SEEMS WAY TOO SLOW -> CANNOT POSSIBLY SCALE...
      uint64_t sleep_tot=0;
      unsigned sleep_inc=1;
      while(useSubLattice && (colorcnt==0)){
        bool updated=1;
        for(int i=0; i<ntrajectories; i++) {
          if(officialSyncStatus[i].length > 0){
            updated=0;
            if(sleep_tot >= 60000){
              std::cout<<" SPLICER WAITING TOO LONG FOR SYNC of itraj "<<i
                      <<" - SOMETHING IS WRONG!!"<<std::endl;
            }
          }
        }
        if(updated){
          break;
        }else{
          std::this_thread::sleep_for(std::chrono::microseconds(sleep_inc));
          sleep_tot+=sleep_inc;
          //if(sleep_tot > 10) sleep_inc+=sleep_inc;
          updateStores();
          processRecv();
          KMC();
          processSend();
        }
      }

      // SLD 'Update' and add item to trajSLlist:
      std::vector<Label> newitem;
      for(int i=0; i<ntrajectories; i++){
        if(SLDTrajectory[i].color == currentcolor) {
          // Determine label before the 'color' was finished:
          Label oldLabel = sldlabels[i];
          // Determine label after the 'color' was finished:
          Label newLabel = SLDTrajectory[i].back().label;
          // Push value for trajSL.out:
          if(colorcnt==0) newitem.push_back(oldLabel);
          // Update the 'old' label for next time around:
          sldlabels[i] = newLabel;
          // Need to schedule sync tasks for neighbors if
          // the label changed from a transition (not from a sync):
          if((oldLabel != newLabel) && (sldtrans[i])){
            sldtrans[i]=0;
            // Update 'officialSyncStatus'
            if(useSubLattice && (ntrajectories>1)) {
              needsync=1;
              std::map<std::vector<int>, unsigned> sln = officialSyncStatus[i].SLneighs;
              std::vector <unsigned> neighs_warned;
              for(auto it=sln.begin(); it!=sln.end(); it++) {
                //std::cout<<"ADD CHANGE in itrajectory "<<i<<" TO jtrajectory: "<<it->second<<std::endl;
                //std::cout<<"ADD CHANGE, vector: "<<it->first[0]<<","<<it->first[1]<<","<<it->first[2]<<std::endl;
                // Warn each unique neighbor once:
                bool found = (std::find(neighs_warned.begin(), neighs_warned.end(), it->second) != neighs_warned.end());
                if(!found){
                  officialSyncStatus[it->second].addnew(i,oldLabel,newLabel);
                  //std::cout<<"DONE ADDING CHANGE."<<std::endl;
                  //officialSyncStatus[it->second].print(it->second);
                  neighs_warned.push_back(it->second);
                }
              }
            }
          }
        }else{
          if(SLDTrajectory[i].color==((currentcolor+1)%ncolors)){
            if(officialSyncStatus[i].length > 0) ovrdDelay=1;
          }
          if(colorcnt==0) newitem.push_back(SLDTrajectory[i].back().label);
        }
      }

      currentcolor=(currentcolor+1)%ncolors;
      if(outputvol > 0)
        std::cout<<" -- CURRENT COLOR = "<<currentcolor<<std::endl;

      if(useSubLattice && (colorcnt==0)) trajSLlist.push_back(newitem);

    }

  }

};

void SLParSpliceSplicer::KMC(){
  if(!useKmcPredict) return; // HACK: Skip KMC for now (RJZ)
  // Use domainList: Loop in random order.
  int rdom = 0; //std::rand() % ndomains;
  int flength = 0;
  for(int idom=0; idom<ndomains; idom++){
    int idomain = idom; //(idom + rdom) % ndomains;
    if(domainList[idomain].nKMCPred<100) {
      //generate a virtual trajectory
      predictions.clear();
      KMCSchedule(predictions, idomain, flength);
      for(auto it=predictions.begin(); it!=predictions.end(); it++) {
        domainList[idomain].predm[it->first].insert(it->second);
      }
      domainList[idomain].nKMCPred++;
      //break if we have reached the prediction time
      //if(flength>=predictionTime*ndomains) break;
    }
  }
};

void SLParSpliceSplicer::writeSL(){
  // Loop through items in trajSLlist
  while(trajSLlist.size()>0){
    std::vector<Label> labels = trajSLlist.front();
    std::stringstream ss;
    // write the global time:
    //ss<<(time);
    // Write statel labels
    for(std::vector<Label>::iterator it=labels.begin(); it!=labels.end(); it++) {
      ss<<" "<<(*it);
    }
    ss<<"\n";
    outTrajSL<<ss.str();
    outTrajSL.flush();
    trajSLlist.pop_front();
  }
}

void SLParSpliceSplicer::report(){
  //report at given intervals
  if(std::chrono::high_resolution_clock::now() - lastReport> reportDelay  ) {
    Timer t;
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server reporting ";

    //output timings
    outTime<<std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-start).count()+carryOverTime<<" "<<usedBlocks<<" "<<producedBlocks<<std::endl;

    // Update trajSL.out (if applicable):
    if(useSubLattice) writeSL();

    for(int itrajectory=0; itrajectory<ntrajectories; itrajectory++) {

      //output trajectory
      int iVisit=0;
      Visit back=SLDTrajectory[itrajectory].back();
      SLDTrajectory[itrajectory].pop_back();

      std::stringstream ss;
      for(auto it=SLDTrajectory[itrajectory].visits.begin(); it!=SLDTrajectory[itrajectory].visits.end(); it++) {
        ss<<" "<<it->label<<" "<<it->duration<<" "<<itrajectory<<"\n";
        iVisit++;
      }
      outTraj<<ss.str();
      outTraj.flush();

      SLDTrajectory[itrajectory].clear();
      SLDTrajectory[itrajectory].appendVisit(back);

    }

    lastReport=std::chrono::high_resolution_clock::now();
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server reporting done ";
    if(outputvol > 1)
      std::cout<<"SPLICER DONE REPORTING "<<t.stop()<<std::endl<<std::flush;
  }
};

void SLParSpliceSplicer::checkpoint(bool now){
  //checkpoint at given intervals
  if(now or std::chrono::high_resolution_clock::now() - lastCheckpoint> checkpointDelay  ) {
    Timer t;
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server checkpointing ";
    //checkpoint();
    outTraj.close();
    outTime.close();

    //copy consistent progress files
    boost::filesystem::copy_file("./traj.out","./traj.out.chk",boost::filesystem::copy_option::overwrite_if_exists);
    boost::filesystem::copy_file("./times.out","./times.out.chk",boost::filesystem::copy_option::overwrite_if_exists);

    outTraj.open("./traj.out", std::ios::app | std::ios::out);
    outTime.open("./times.out", std::ios::app | std::ios::out);


    std::ofstream ofs("./Splicer.chk");
    // save data to archive
    {
      boost::archive::text_oarchive oa(ofs);
      // write class instance to archive
      oa << *this;
      // archive and stream closed when destructors are called
    }

    if(outputvol > 0) std::cout<<"CHECKPOINTING "<<t.stop()<<std::endl<<std::flush;
    lastCheckpoint=std::chrono::high_resolution_clock::now();
    //BOOST_LOG_SEV(lg,debug) <<"#Splicer::server checkpointing done";
  }
};


void SLParSpliceSplicer::KMCSchedule( std::unordered_map<Label, unsigned > & visits, unsigned idomain, int &flength){

  //keep track of which segment in the database we already used
  std::unordered_map<Label, int> consumedSegments;
  double w=1;
  int dlength = 0; //can reach predictionTime

  for(int icolor=0; icolor<ncolors; icolor++){
    int color = (currentcolor + icolor) % ncolors;
    unsigned itraj = domainList[idomain].trajectories[color];
    Trajectory traj = SLDTrajectory[itraj];

    // Don't sample from a label that will likely change
    if(useSubLattice && (officialSyncStatus[itraj].length>0)){
      // Clear predm if we need to synchronize the current color?
      if(icolor==0){
        domainList[idomain].predm.clear();
        domainList[idomain].nKMCPred=0;
      }
      break;
    }

    Label c=traj.back().label;
    unsigned ilength=traj.blocksThisCycle;  //can reach minPerCycle
    std::pair<Label, unsigned> s;
    {
      while(true) {
        //"virtually" consume segment from the database
        auto key=std::make_pair(c,defaultFlavor);
        if(segmentDB.count(c,defaultFlavor)>0 and consumedSegments[c]< segmentDB.count(c,defaultFlavor) ) {
          Trajectory &t=segmentDB.db[key][consumedSegments[c]];
          consumedSegments[c]+=1;
          c=t.back().label;
          ilength+=segblocks;
          if(ilength>=minPerCycle) break;
        }
        //generate segment with BKL
        else{
          s=markovModel.sampleBKL(c, defaultVisitDuration, w);
          int duration=s.second;
          Visit v;
          v.label=c;
          //do not exceed the prediction horizon
          v.duration=( (duration+ilength)>minPerCycle ? (minPerCycle-ilength) : duration );
          ilength+=v.duration;
          dlength+=v.duration;
          flength+=v.duration;
          //visits.push_back(v);
          visits[v.label]+=v.duration; // RJZ: Should we be using += if v.label is not in visits yet??
          c=s.first;
          //break if weight becomes too low or we reached the cycle time
          if(ilength>=minPerCycle or w<wMin) break;
          if(dlength>=predictionTime) break;
        }
      }
    }
    //break if we have reached this domains share of the prediction time
    if(dlength>=(predictionTime)) break;
  }

};

bool SLParSpliceSplicer::spliceOne(int itrajectory){

  if(SLDTrajectory[itrajectory].blocksThisCycle >= minPerCycle) return false;

  //BOOST_LOG_SEV(lg,trace) <<"#Splicer::splice ";
  //add segments to the official trajectory
  Label end=SLDTrajectory[itrajectory].back().label;
  //BOOST_LOG_SEV(lg,info) <<"#Splicer Splice Current end: "<<end;

  auto key=std::make_pair(end,defaultFlavor);

  if( segmentDB.count(end,defaultFlavor)> 0 ) {

    if(0){
      Trajectory t;
      if((ntrajectories > 1) && (ncolors > 1)) {
        // If splicing multiple trajectories, call 'front_split'...
        // Should only take one visit at a time, and need to split that visit
        // if it is too long
        int needblocks = minPerCycle;
        needblocks -= SLDTrajectory[itrajectory].blocksThisCycle;
        segmentDB.front_split(end,defaultFlavor,t,needblocks);
      }else{
        // Removed the entire trajectory at 'end'.. we can use everything
        segmentDB.front(end,defaultFlavor,t);
        segmentDB.pop_front(end,defaultFlavor);
      }
      usedBlocks+=t.duration();
      usedBlocksSD[itrajectory]+=t.duration();
      SLDTrajectory[itrajectory].blocksThisCycle += t.duration();
      //unusedBlocks-=t.size();
      SLDTrajectory[itrajectory].splice(t);
      end=SLDTrajectory[itrajectory].back().label;
      key=std::make_pair(end,defaultFlavor);
      //std::cout<<"#Splicer Splice Current end: "<<end<<" for itrajectory "<<itrajectory<<"\n";
      //std::cout<<"itrajectory "<<itrajectory<<" now has blocksThisCycle = "
      //<<SLDTrajectory[itrajectory].blocksThisCycle<<"\n";
      //BOOST_LOG_SEV(lg,trace) <<"#Splicer Splice Current end: "<<end<<" "<<t.length;
      return true;
    }else{
      Trajectory t;

      // Removed the entire trajectory at 'end'.. we can use everything
      segmentDB.front(end,defaultFlavor,t);
      segmentDB.pop_front(end,defaultFlavor);

      usedBlocks+=t.duration();
      usedBlocksSD[itrajectory]+=t.duration();
      SLDTrajectory[itrajectory].blocksThisCycle += t.duration();
      //unusedBlocks-=t.size();
      SLDTrajectory[itrajectory].splice(t);
      end=SLDTrajectory[itrajectory].back().label;
      key=std::make_pair(end,defaultFlavor);

      //BOOST_LOG_SEV(lg,trace) <<"#Splicer Splice Current end: "<<end<<" "<<t.length;

      if((outputvol > 1) && (SLDTrajectory[itrajectory].blocksThisCycle >= minPerCycle)){
        std::cout<<"#Splicer Current end: "<<end<<" for itrajectory "<<itrajectory
          <<" now has blocksThisCycle = "<<SLDTrajectory[itrajectory].blocksThisCycle<<"\n";
      }

      return true;
    }
  }
  else{
    return false;
  }

};

void SLParSpliceSplicer::output(){

  //output timings
  outTime<<std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-start).count()+carryOverTime<<" "<<usedBlocks<<" "<<producedBlocks<<std::endl;

  // Update trajSL.out (if applicable):
  if(useSubLattice) writeSL();

  //loop through trajectories
  for(int itrajectory=0; itrajectory<ntrajectories; itrajectory++) {

    //output trajectory
    int iVisit=0;
    Visit back=SLDTrajectory[itrajectory].back();
    SLDTrajectory[itrajectory].pop_back();

    std::stringstream ss;
    for(auto it=SLDTrajectory[itrajectory].visits.begin(); it!=SLDTrajectory[itrajectory].visits.end(); it++) {
      ss<<" "<<it->label<<" "<<it->duration<<" "<<itrajectory<<"\n";
      iVisit++;
    }
    outTraj<<ss.str();
    outTraj.flush();

    SLDTrajectory[itrajectory].clear();
    SLDTrajectory[itrajectory].appendVisit(back);

  }

};
