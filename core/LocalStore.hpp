//
//  LocalStore.hpp
//  ParSplice-refact
//
//  Created by Danny Perez on 1/9/17.
//  Copyright © 2017 dp. All rights reserved.
//

#ifndef LocalStore_hpp
#define LocalStore_hpp

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sstream>
#include <unordered_map>
#include <map>
#include <vector>
#include <string>

//#include <leveldb/db.h>
#include <db_cxx.h>
#include <dbstl_map.h>

#include "HCDS.hpp"

#define DBKEY_NOTFOUND 1
#define KEY_NOTFOUND 2


class AbstractDataStore {
public:
virtual int put(unsigned int dbKey, uint64_t &key, Rd &data)=0;
virtual int get(unsigned int dbKey, uint64_t &key, Rd &data)=0;
virtual unsigned int count(unsigned int dbKey, uint64_t &key)=0;
virtual int erase(unsigned int dbKey, uint64_t &key)=0;
};

//abstract base class for the local data store
class AbstractLocalDataStore : public AbstractDataStore {
public:
AbstractLocalDataStore();
virtual ~AbstractLocalDataStore(){
};
virtual int put(unsigned int dbKey, uint64_t &key, Rd &data)=0;
virtual int get(unsigned int dbKey, uint64_t &key, Rd &data)=0;
virtual int createDatabase(unsigned int dbKey, bool allowDuplicates, bool eraseOnGet)=0;
virtual int initialize(std::string homeDir, std::string baseName)=0;
virtual int sync()=0;
virtual int erase(unsigned int dbKey, uint64_t &key)=0;

void setMaximumSize(unsigned long maxSize_);
virtual unsigned int count(unsigned int dbKey, uint64_t &key);
virtual std::set<uint64_t> availableKeys(unsigned int dbKey);
virtual std::multimap< std::pair<unsigned int, uint64_t>, Rd> purge();
virtual void touch(unsigned int dbKey, uint64_t &key);

protected:

std::unordered_map<unsigned int, std::set<uint64_t> > storedKeys;
MRU< std::pair<unsigned int, uint64_t> > mru;
unsigned long maxSize;
unsigned long currentSize;

std::unordered_map< unsigned int, std::pair<bool,bool> > dbAttributes;
};


#if 0
class LDBLocalDataStore : public AbstractLocalDataStore {
public:
LDBLocalDataStore();
~LDBLocalDataStore();

int initialize(std::string homeDir, std::string baseName);
virtual int put(unsigned int dbKey, uint64_t &key, Rd &data);
virtual int get(unsigned int dbKey, uint64_t &key, Rd &data);
int createDatabase(unsigned int dbKey, bool allowDuplicates, bool eraseOnGet);
int sync();
virtual int erase(unsigned int dbKey, uint64_t &key);


private:
std::string u2sKeyConv(uint64_t &key);
uint64_t s2uKeyConv(std::string &key);
std::string homeDir;
std::string baseName;
unsigned long scratchSize;
std::unordered_map<unsigned int, leveldb::DB* > dbm;
};
#endif

class BDBLocalDataStore : public AbstractLocalDataStore {
public:
BDBLocalDataStore();
~BDBLocalDataStore();

int initialize(std::string homeDir, std::string baseName);
virtual int put(unsigned int dbKey, uint64_t &key, Rd &data);
virtual int get(unsigned int dbKey, uint64_t &key, Rd &data);
int createDatabase(unsigned int dbKey, bool allowDuplicates, bool eraseOnGet);
int sync();
virtual int erase(unsigned int dbKey, uint64_t &key);


private:
Rd u2RdKeyConv(uint64_t &key);
uint64_t rd2uKeyConv(Rd &key);
std::string homeDir;
std::string baseName;
unsigned long scratchSize;
DbEnv *env;
std::unordered_map<unsigned int, Db* > dbm;
};


class STLLocalDataStore : public AbstractLocalDataStore {
public:
STLLocalDataStore();

int initialize(std::string homeDir, std::string baseName);
virtual int put(unsigned int dbKey, uint64_t &key, Rd &data);
virtual int get(unsigned int dbKey, uint64_t &key, Rd &data);
int createDatabase(unsigned int dbKey, bool allowDuplicates, bool eraseOnGet);
int sync();
virtual int erase(unsigned int dbKey, uint64_t &key);

private:
std::unordered_map< unsigned int, std::unordered_multimap< uint64_t, Rd> > dbm;
};


class HierachicalLocalDataStore {

std::list<AbstractLocalDataStore> stores;
};

#endif /* LocalStore_hpp */
