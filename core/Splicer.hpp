/*
   Copyright (c) 2016, Los Alamos National Security, LLC
   All rights reserved.
   Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

   Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ParSplice__Splicer__
#define __ParSplice__Splicer__

#include <new>
#include <stdio.h>
#include <limits>
#include <memory>
#include <atomic>
#include <future>
#include <deque>
#include <chrono>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>

#include <mpi.h>
#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/utility/setup/settings.hpp>
#include <boost/log/utility/setup/from_settings.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/unordered_set.hpp>
#include <boost/unordered_map.hpp>
#include <boost/optional.hpp>
#include <boost/bimap/bimap.hpp>
#include <boost/bimap/unordered_set_of.hpp>
#include <boost/bimap/unordered_multiset_of.hpp>
#include <boost/bimap/vector_of.hpp>
#include <boost/bimap/multiset_of.hpp>
#include <boost/bimap/support/lambda.hpp>
#include <boost/timer/timer.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/random/random_device.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/functional/hash/hash.hpp>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/unordered_set.hpp>
#include <boost/serialization/list.hpp>
#include <boost/filesystem.hpp>

#include "Types.hpp"
#include "Task.hpp"
#include "Pack.hpp"
#include "Constants.hpp"
#include "Validator.hpp"
#include "DDS.hpp"
#include "CustomTypes.hpp"
#include "TaskManager.hpp"
#include "DefaultInput.hpp"

struct SchedulerState {
public:
  bool operator<( const SchedulerState& other ) const {
    //we want states with the same label to be considered equal
    if(label==other.label) {
      return false;
    }
    //to break ties in size, use labels
    if( counts.size() == other.counts.size() ) {
      return label<other.label;
    }
    //sort by size
    return counts.size()<other.counts.size();
  };

  Label label;
  std::multiset<unsigned> counts;
};

class ParSpliceSplicer {

public:
  ParSpliceSplicer(boost::property_tree::ptree &config);
  ParSpliceSplicer(MPI_Comm splicerComm_, AbstractDDS *minimaStore_, std::set<int> children_, boost::property_tree::ptree &config, MPI_Comm workerComm);
  ~ParSpliceSplicer();
  virtual void addsystem_byfile(boost::property_tree::ptree &config);
  void server();
  void updateStores();
  virtual void processRecv();
  virtual void processSend();
  virtual void splice();
  virtual void KMC();
  virtual void report();
  virtual void checkpoint(bool now=false);
  virtual void KMCSchedule( std::unordered_map<Label, unsigned > & visits);
  std::unordered_map<Label, int> KMCScheduleAbs( std::unordered_map<Label, unsigned > & visits);
  virtual bool spliceOne();
  virtual void output();

  template<class Archive>
  void save(Archive & ar, const unsigned int version) const {
    // note, version is always the latest when saving
    ar & usedBlocks;
    ar & unusedBlocks;
    long int co=std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-start).count()+carryOverTime;
    ar & co;
    ar & segmentDB;
    ar & markovModel;
    ar & officialTrajectory;
  };

  template<class Archive>
  void load(Archive & ar, const unsigned int version){
    ar & usedBlocks;
    ar & unusedBlocks;
    ar & carryOverTime;
    ar & producedBlocks;
    ar & segmentDB;
    ar & markovModel;
    ar & officialTrajectory;
  };
  BOOST_SERIALIZATION_SPLIT_MEMBER()

  SegmentDatabase validatedDB;
  Trajectory officialTrajectory;

protected:
  bool flushOnModify;
  ModifierType modifier;
  DriverHandleType *driver;
  AbstractDDS *minimaStore;

  SegmentDatabase segmentDB;
  std::set<int> children;
  Validator validator;
  TransitionStatistics markovModel;

  unsigned long usedBlocks;
  unsigned long unusedBlocks;
  unsigned long wastedBlocks;
  unsigned long producedBlocks;
  unsigned long carryOverTime;

  unsigned predictionTime;
  unsigned defaultVisitDuration;
  unsigned predictionGranularity;
  unsigned batchSize;
  unsigned maxTaskMesgSize;
  double wMin;

  unsigned outputvol;
  unsigned int validatorTimeout;

  std::map< std::pair<int,int>, std::map<std::string,std::string> > taskParameters;
  std::unordered_map<Label, unsigned > predictions;

  MPI_Comm splicerComm;
  int recvCompleted;
  int sendCompleted;
  MPI_Status recvStatus;
  MPI_Status sendStatus;
  MPI_Request incomingRequest;
  MPI_Request outgoingRequest;
  int broadcastCompleted;
  std::vector<char> rbuf;
  int rbufSize;
  //MPI_Request broadcastRequests[1000];
  std::vector<MPI_Request> broadcastRequests;
  std::vector<std::vector<char> > sBuf;

  boost::log::sources::severity_logger< boost::log::trivial::severity_level > lg;

  std::chrono::high_resolution_clock::time_point start;
  std::chrono::milliseconds broadcastDelay;
  std::chrono::milliseconds reportDelay;
  std::chrono::milliseconds checkpointDelay;
  std::chrono::minutes runTime;

  std::chrono::high_resolution_clock::time_point lastBroadcast;
  std::chrono::high_resolution_clock::time_point lastReport;
  std::chrono::high_resolution_clock::time_point lastCheckpoint;

  std::ofstream outTraj;
  std::ofstream outTime;

  int defaultFlavor;
  int STATE_DBKEY;
  uint64_t batchId;
  bool die;
  bool killed;

};





class SLParSpliceSplicer : public ParSpliceSplicer {

public:

  // Constructor:
  SLParSpliceSplicer(MPI_Comm splicerComm_, AbstractDDS *minimaStore_, std::set<int> children_, boost::property_tree::ptree &config, MPI_Comm workerComm);
  ~SLParSpliceSplicer();
  void addsystem_byfile(boost::property_tree::ptree &config, int itrajectory);
  void processRecv();
  void sendSyncBatch(std::set<unsigned> syncset);
  void processSend();
  void spliceSLD(int itrajectory);
  void splice();
  void KMC();
  void writeSL();
  void report();
  void checkpoint(bool now=false);
  void KMCSchedule( std::unordered_map<Label, unsigned > & visits, unsigned idomain, int &flength);
  bool spliceOne(int itrajectory);
  void output();

  template<class Archive>
  void save(Archive & ar, const unsigned int version) const {
    // note, version is always the latest when saving
    ar & usedBlocks;
    for(int i=0; i<ntrajectories; i++) {
      ar & usedBlocksSD[i];
    }
    ar & unusedBlocks;
    long int co=std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now()-start).count()+carryOverTime;
    ar & co;
    ar & segmentDB;
    ar & markovModel;
    for(int i=0; i<ntrajectories; i++) {
      ar & SLDTrajectory[i];
    }
  };

  template<class Archive>
  void load(Archive & ar, const unsigned int version){
    ar & usedBlocks;
    for(int i=0; i<ntrajectories; i++) {
      ar & usedBlocksSD[i];
    }
    ar & unusedBlocks;
    ar & carryOverTime;
    ar & producedBlocks;
    ar & segmentDB;
    ar & markovModel;
    for(int i=0; i<ntrajectories; i++) {
      ar & SLDTrajectory[i];
    }
  };
  BOOST_SERIALIZATION_SPLIT_MEMBER()

  SegmentDatabase validatedDB;

  // Vector of trajectories
  std::vector<Trajectory> SLDTrajectory;

  // Map of color index to a vector of trajectories with that color
  std::map<unsigned, std::vector<unsigned>> colorMap;

  // Vector of Domian objects. Each Domain object stores a vector of the
  // trajectory indices it owns (the vector should be indexed by color)
  std::vector <Domain> domainList;

  // one for each Sub-domain (SD) in SLParSplice
  std::vector<SyncStatus> officialSyncStatus;
  bool     useKmcPredict;
  unsigned trajSLwait;
  unsigned outputvol;
  unsigned ntrajectories;       //rjz// Number of trajectories (or SDs)
  unsigned ndomains;            //rjz// How many domains are trajectories split into
  unsigned ncolors;             //rjz// How many trajectory 'colors' to cycle through
  unsigned currentcolor;        //rjz// What 'color' are we currently SPLICING
  unsigned minPerCycle;         //rjz// How many blocks MUST be spliced in a tajectory each cycle
  unsigned colorcnt;            //rjz// Count of completed colors (modulo trajSLwait)
  int      segblocks;
  bool     useSubLattice;       //rjz// Turn On Sub-lattice Decomposition
  bool     ovrdDelay;           //rjz// Overide the delay between broadcasts
  bool     check1st;            //rjz// Are we sending reference labels during sync?
  std::vector<int> nxyzSL;      //rjz// Set the number of sublattice domains
  std::vector<Label> sldlabels; //rjz// store all sld labels
  std::vector<bool> sldtrans;   //rjz// did this sld have a trans

  //rjz// Use this list to write 'trajSL.out':
  std::list< std::vector<Label> >  trajSLlist;

protected:
  std::vector <unsigned long> usedBlocksSD; // How many blocks used by each SD
  std::ofstream outTrajSL;

};


#endif /* defined(__ParSplice__Splicer__) */
