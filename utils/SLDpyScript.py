#!/usr/bin/env python

import sys            # READS in LAMMPS input file, and performs sublattice
import subprocess     # Decomposition into multiple Sublattice Domains (SLDs)
import re             #
                      # Syntax:
                      #   python SLDpyScript.py
                      #       <FILE> <nx> <ny> <nz> <dbuf> <dfix> <dvac>
                      #   --> Where FILE = Global input file,
                      #       nx, ny, nz = SLD divisions in x,y,z directions,
                      #       dbuf       = SLD "buffer" distance, and
                      #       dfix       = SLD "fixed" distance
                      #
                      # Author: R.J. Zamora (rjzamora@lanl.gov)
                      #
                      #     Last modified: June 21, 2017

##============================================================================##
##---------------------------------- writelmp --------------------------------##
##============================================================================##

def writelmp(outname,simbox,natoms,ntypes,types,xyz):

    with open(outname,'w') as fout:

        [xlo, xhi, ylo, yhi, zlo, zhi] = simbox
        dx = xlo
        dy = ylo
        dz = zlo

        fout.write('  LAMMPS Data file, converted from CLSMAN format\n\n');
        fout.write(\
            '  %f  %f   xlo xhi\n  %f  %f   ylo yhi\n  %f  %f   zlo zhi\n' \
            %(xlo-dx,xhi-dx,ylo-dy,yhi-dy,zlo-dz,zhi-dz))
        fout.write('\n\n')
        fout.write('     %d atom types\n\n' %(int(ntypes)));
        fout.write('     %d atoms\n\n' %(int(natoms)))
        fout.write('Atoms\n   Atom ID, Atom type, X, Y, Z\n')

        #Atom coordinates
        for atomid in xrange(natoms):
            itype = types[atomid]
            [x, y, z] = xyz[atomid]
            fout.write('%5i  %5i  %20.15f  %20.15f  %20.15f\n' %(atomid+1, \
                itype, float(x-dx), float(y-dy), float(z-dz)))

    return

################################################################################
##============================================================================##
##------------------------------------ MAIN ----------------------------------##
##============================================================================##
################################################################################

if len(sys.argv) <> 8:
    print "Syntax: python SLDpyScript.py <FILE> <nx> <ny> <nz> <dbuf> <dfix> <vac>"
    sys.exit(1)

# Set global filename, and x,y,z divisions:
gfilename = sys.argv[1]
nx        = int(sys.argv[2])
ny        = int(sys.argv[3])
nz        = int(sys.argv[4])
dbufSL    = float(sys.argv[5])
dfixSL    = float(sys.argv[6])
vac       = float(sys.argv[7])
nxyzSL    = [nx, ny, nz]
ntraj     = nx * ny * nz

print "dbufSL, dfixSL =",dbufSL, dfixSL

# Temporary Hack:
if False:
    for i in xrange(ntraj):
        newfilename = gfilename + "." + str(i)
        subprocess.call(["cp",gfilename,newfilename])
    sys.exit(0)

# Read Global Input:
boxhi = [0.0, 0.0, 0.0]
boxlo = [0.0, 0.0, 0.0]
natoms = 0
ntypes = 0
types  = []
xyz    = []
with open(gfilename,'r') as fidi:
    startatoms = False
    for line in fidi:
        arglist = line.split()
        #arglist = re.split(' |,',line)
        #print arglist
        if len(arglist) < 1: continue
        if not startatoms:
            if   (len(arglist) > 3) and (arglist[2] == "xlo"):
                boxlo[0] = float(arglist[0])
                boxhi[0] = float(arglist[1])
                #print "boxlo, boxhi [0]: ",boxlo[0],boxhi[0]
            elif (len(arglist) > 3) and (arglist[2] == "ylo"):
                boxlo[1] = float(arglist[0])
                boxhi[1] = float(arglist[1])
                #print "boxlo, boxhi [1]: ",boxlo[1],boxhi[1]
            elif (len(arglist) > 3) and (arglist[2] == "zlo"):
                boxlo[2] = float(arglist[0])
                boxhi[2] = float(arglist[1])
                #print "boxlo, boxhi [2]: ",boxlo[2],boxhi[2]
            elif (len(arglist) > 2) and (arglist[1] == "atom") \
                                    and (arglist[2] == "types"):
                ntypes = int(arglist[0])
                #print "ntypes: ",ntypes
            elif (len(arglist) > 1) and (arglist[1] == "atoms"):
                natoms = int(arglist[0])
                #print "natoms: ",natoms
        if startatoms:
            it = int(arglist[1])
            x  = float(arglist[2])
            y  = float(arglist[3])
            z  = float(arglist[4])
            if nxyzSL[0] > 1:
                while (x >= boxhi[0]): x -= (boxhi[0]-boxlo[0])
                while (x <  boxlo[0]): x += (boxhi[0]-boxlo[0])
            if nxyzSL[1] > 1:
                while (y >= boxhi[1]): y -= (boxhi[1]-boxlo[1])
                while (y <  boxlo[1]): y += (boxhi[1]-boxlo[1])
            if nxyzSL[2] > 1:
                while (z >= boxhi[2]): z -= (boxhi[2]-boxlo[2])
                while (z <  boxlo[2]): z += (boxhi[2]-boxlo[2])
            types.append(it)
            xyz.append([x,y,z])

        if (arglist[0] == "Atom") and (arglist[1] == "ID,"): startatoms = True

# General SSD Parameters:
sld_dims = [0.0,0.0,0.0]
for i in xrange(3):
    sld_dims[i] = float(boxhi[i]-boxlo[i]) / float(nxyzSL[i])
    if(sld_dims[i] < (2.0 * dbufSL)):
        if(nxyzSL[i]>1): print "WARNING: SLD is too small!!!"
print "SLD Dimensions to be:", sld_dims[0], sld_dims[1], sld_dims[2]

# Loop through Sub-lattice:
itrajectory = 0
ytrans = [False, False, False]
for k in xrange(nxyzSL[2]):
    zlo = (k + 0) * sld_dims[2]
    zhi = (k + 1) * sld_dims[2]
    ytrans[2] = False
    if nxyzSL[2] > 1:
        zlo -= (dbufSL + dfixSL)
        zhi += (dbufSL + dfixSL)
        ytrans[2] = True
    for j in xrange(nxyzSL[1]):
        ylo = (j + 0) * sld_dims[1]
        yhi = (j + 1) * sld_dims[1]
        ytrans[1] = False
        if nxyzSL[1] > 1:
            ylo -= (dbufSL + dfixSL)
            yhi += (dbufSL + dfixSL)
            ytrans[1] = True
        for i in xrange(nxyzSL[0]):
            xlo = (i + 0) * sld_dims[0]
            xhi = (i + 1) * sld_dims[0]
            ytrans[0] = False
            if nxyzSL[0] > 1:
                xlo -= (dbufSL + dfixSL)
                xhi += (dbufSL + dfixSL)
                ytrans[0] = True
            lnatoms = 0
            ltypes  = []
            lxyz    = []
            for iloop in xrange(len(xyz)):
                coord = xyz[iloop]
                # If periodic direction, move possible atoms accross pbc:
                for idim in xrange(3):
                    if not ytrans[idim]: continue
                    delt = boxhi[idim]-boxlo[idim]
                    if idim == 0:
                      lo = xlo; hi = xhi
                    if idim == 1:
                      lo = ylo; hi = yhi
                    if idim == 2:
                      lo = zlo; hi = zhi
                    if (coord[idim] <  lo): coord[idim] += delt
                    if (coord[idim] >= hi): coord[idim] -= delt
                # Continue if this atom doesn't belong:
                if (coord[0] < xlo) or (coord[0] >= xhi): continue
                if (coord[1] < ylo) or (coord[1] >= yhi): continue
                if (coord[2] < zlo) or (coord[2] >= zhi): continue
                lxyz.append(coord)
                ltypes.append(types[iloop])
                lnatoms += 1
            # Write Input file for this SLD:
            simbox = [xlo, xhi, ylo, yhi, zlo, zhi]
            # Add vacuum:
            if nxyzSL[0] > 1:
                simbox[0] -= vac; simbox[1] += vac;
            if nxyzSL[1] > 1:
                simbox[2] -= vac; simbox[3] += vac;
            if nxyzSL[2] > 1:
                simbox[4] -= vac; simbox[5] += vac;
            newfilename = gfilename + "." + str(itrajectory)
            writelmp(newfilename,simbox,lnatoms,ntypes,ltypes,lxyz)
            itrajectory += 1
