/*
   Copyright (c) 2016, Los Alamos National Security, LLC
   All rights reserved.
   Copyright 2016. Los Alamos National Security, LLC. This software was produced under U.S. Government contract DE-AC52-06NA25396 for Los Alamos National Laboratory (LANL), which is operated by Los Alamos National Security, LLC for the U.S. Department of Energy. The U.S. Government has rights to use, reproduce, and distribute this software.  NEITHER THE GOVERNMENT NOR LOS ALAMOS NATIONAL SECURITY, LLC MAKES ANY WARRANTY, EXPRESS OR IMPLIED, OR ASSUMES ANY LIABILITY FOR THE USE OF THIS SOFTWARE.  If software is modified to produce derivative works, such modified software should be clearly marked, so as not to confuse it with the version available from LANL.

   Additionally, redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
   1.      Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   2.      Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
   3.      Neither the name of Los Alamos National Security, LLC, Los Alamos National Laboratory, LANL, the U.S. Government, nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY LOS ALAMOS NATIONAL SECURITY, LLC AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL LOS ALAMOS NATIONAL SECURITY, LLC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */



#include <iostream>
#include <algorithm>


#include <mpi.h>
#include <vector>
#include <ostream>
#include <streambuf>
#include <sstream>


#include <boost/filesystem.hpp>

#include <chrono>
#include <thread>


//This file contains the type definitions. Edit this file to change to LAMMPS classes
#include "CustomTypes.hpp"


#include "LocalStore.hpp"

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>



std::pair<double,double> bench(size_t datasize, int nput ){

	std::vector<char> data(datasize,0);

	boost::filesystem::remove_all("./db/");
	BDBLocalDataStore store;
	store.initialize("./db/","db");
	store.createDatabase(0, false, false);
	uint64_t key=1234;

	store.sync();

	double tp;
	double ts;
	for(int j=0; j<2; j++) {
		{
			Timer t;
			for(int i=0; i<nput; i++) {
				store.put(0,key,data);
				key++;
			}
			tp=t.stop()/double(nput);

		}
		{
			Timer t;
			store.sync();
			ts=t.stop();
		}
		//std::cout<<tp<<" "<<ts<<std::endl;
	}
	return std::make_pair(tp,ts);
};

int main(int argc, char * argv[]) {
	//std::cout << "ParSplice-dbbenchmark:\n";


	MPI_Init(&argc, &argv);

	int rank;
	int parent;
	int nranks;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nranks);


#if 1
	//std::cout << "ParSplice-refact\n"<<std::endl;


	size_t datasize=1;

	while(datasize<1e6) {
		int nput=1;
		while(nput<1e7 and datasize*nput<1e10) {
			{
				auto p=bench( datasize,  nput );
				std::cout<<datasize<<" "<<nput<<" "<<p.first<<" "<<p.second<<std::endl;
			}
			nput*=3;
		}
		datasize*=3;
	}




	MPI_Finalize();
#endif




}
