cmake_minimum_required(VERSION 3.5)
project (ParSplice)

enable_testing()

# Cmake modules/macros are in a subdirectory to keep this file cleaner
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/CMakeModules)

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CXX_FLAGS)
  #release comes with -O3 by default
  set(CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel." FORCE)
endif(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CXX_FLAGS)


if(STATIC_LINK_FLAG)
  set(CMAKE_EXE_LINKER_FLAGS ${STATIC_LINK_FLAG})
endif(STATIC_LINK_FLAG)

# Enable C++ and ensure C++11
enable_language(CXX)
set(CMAKE_CXX_STANDARD 11) # C++11...
set(CMAKE_CXX_STANDARD_REQUIRED ON) #...is required...
set(CMAKE_CXX_EXTENSIONS OFF) #...without compiler extensions like gnu++11

# Find python for testing
find_package(PythonInterp 2 REQUIRED)

find_package(DB REQUIRED)
include_directories(${DB_INCLUDE_DIRS})

# Boost: Identify components we need to load from Boost and load them
set(PARSPLICE_BOOST_COMPONENTS random 
                               serialization
			       filesystem
			       system
			       log_setup
			       log
			       thread
			       regex
			       chrono
			       atomic
			       date_time
			       program_options)
set(Boost_USE_STATIC_LIBS ON)    # only find static libs
find_package(Boost 1.58.0 REQUIRED COMPONENTS ${PARSPLICE_BOOST_COMPONENTS})
foreach(mycomponent ${PARSPLICE_BOOST_COMPONENTS})
  list(APPEND PARSPLICE_BOOST_LIBRARIES "Boost::${mycomponent}")
endforeach(mycomponent)

find_package(Eigen3 REQUIRED)
include_directories(${EIGEN3_INCLUDE_DIR})

find_package(Threads REQUIRED)

find_package(MPI REQUIRED)
include_directories(${MPI_C_INCLUDE_PATH})

find_package(NAUTY REQUIRED)
include_directories(${NAUTY_INCLUDE_DIRS})

find_package(LAMMPS REQUIRED)
include_directories(${LAMMPS_INCLUDE_DIRS})

include_directories(core)
include_directories(${CMAKE_INCLUDE_PATH})
link_directories(${CMAKE_LIBRARY_PATH})

file(GLOB PARSPLICE_CORE_FILES "core/*")

set(PARSPLICE_MAIN_FILES ${PARSPLICE_CORE_FILES} main/main.cpp)
set(PARSPLICE_EXTRACT_FILES ${PARSPLICE_CORE_FILES} dbextract/main.cpp)
set(PARSPLICE_MD_FILES ${PARSPLICE_CORE_FILES} md/main.cpp)
set(PARSPLICE_LABEL_FILES ${PARSPLICE_CORE_FILES} label/main.cpp)
#set(PARSPLICE_BENCH_FILES ${PARSPLICE_CORE_FILES} dbbenchmark/main.cpp)

add_executable(parsplice ${PARSPLICE_MAIN_FILES})
add_executable(dbextract ${PARSPLICE_EXTRACT_FILES})
add_executable(parsplice-md ${PARSPLICE_MD_FILES})
add_executable(parsplice-label ${PARSPLICE_LABEL_FILES})
#add_executable(dbbench ${PARSPLICE_BENCH_FILES})

if(EXTRA_FLAGS)
   message(STATUS "Compiling with these extra flags: ${EXTRA_FLAGS}")
   add_definitions(${EXTRA_FLAGS})
endif()

# Base dependencies for all targets
if(EXTRA_LINK_FLAGS)
   message(STATUS "Linking with these extra flags: ${EXTRA_LINK_FLAGS}")
endif()
set(MY_DEPENDENCIES ${CMAKE_THREAD_LIBS_INIT} 
                    ${LAMMPS_LIBRARIES} 
		    ${DB_LIBRARIES} 
		    ${NAUTY_LIBRARIES} 
		    ${PARSPLICE_BOOST_LIBRARIES}
		    ${MPI_CXX_LIBRARIES}
		    ${EXTRA_LINK_FLAGS}
)

# add dependencies for all targets
foreach(mytarget parsplice dbextract parsplice-md parsplice-label)
  # add base dependencies
  target_link_libraries( ${mytarget} ${MY_DEPENDENCIES})
endforeach(mytarget)

include(GNUInstallDirs)
install(TARGETS parsplice DESTINATION ${CMAKE_INSTALL_BINDIR})
install(TARGETS dbextract DESTINATION ${CMAKE_INSTALL_BINDIR})
install(TARGETS parsplice-md DESTINATION ${CMAKE_INSTALL_BINDIR})
install(TARGETS parsplice-label DESTINATION ${CMAKE_INSTALL_BINDIR})
install(FILES node-affinity/mkhosts-slurm-nospawn.py DESTINATION ${CMAKE_INSTALL_BINDIR})

#set(mpi_exe "mpirun")
set(exe_root "${CMAKE_CURRENT_BINARY_DIR}")
file(GLOB test_dirs ${CMAKE_CURRENT_SOURCE_DIR}/tests/*test)
foreach(test_dir ${test_dirs})
    string(REGEX REPLACE ".*/" "" test_name "${test_dir}")
    add_test(NAME ${test_name} COMMAND ${PYTHON_EXECUTABLE} ../runtest.py ${MPIEXEC} ${exe_root} ${test_name} WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/tests/${test_name})
endforeach(test_dir)
